import MessageFormat from "messageformat";

// @ts-ignore
String.prototype.format = function(...args) {
    let formatted = this;
    args.forEach((arg) => {
        Object.entries(arg).map(([key, value]) => {
            formatted = formatted.replace(key, value);
        });
    });
    return formatted;
};

String.prototype.messageFormat = function(args: {[key: string]: any}) {
    const MF = new MessageFormat("ru");

    return MF.compile(this)(args);
};
