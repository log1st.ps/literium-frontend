module.exports = {
    webpack(config, {
        isServer,
    } = {
        isServer: false,
    }) {
        config.module.rules.push({
            test: /\.inline\.svg$/,
            use: [
                {
                    loader: "babel-loader",
                },
                {
                    loader: "react-svg-loader",
                    options: {
                        jsx: true,
                    },
                },
            ],
        });

        config.module.rules.push({
            test: /\.(jpe?g|png|gif|svg|ico|webp)$/,
            exclude: [
                ...(config.exclude || []),
                /\.inline/,
            ],
            use: [
                {
                    loader: require.resolve("url-loader"),
                    options: {
                        limit: 8192,
                        fallback: require.resolve("file-loader"),
                        publicPath: `/_next/static/images/`,
                        outputPath: `${isServer ? "../" : ""}static/images/`,
                        name: "[name]-[hash].[ext]",
                    },
                },
            ],
        });

        return config;
    },
};
