import {AppProps} from "next/app";
import dynamic from "next/dynamic";
import React from "react";
import {Provider} from "react-redux";
import {compose, lifecycle} from "recompose";
import "../implementations";
import "../src/assets/drop.scss";
import {withMobileObserver} from "../src/hoc/withMobileObserver";
import {store} from "../src/store/store";

import "../src/assets/root.scss";
import {handlePopupObservers} from "../src/helpers/observePopup";

const DefaultLayout = dynamic(
    () => import("../src/containers/MainLayoutContainer").then((file) => file.MainLayoutContainer),
);

const InnerApp = ({
    pageProps,
    Component,
}: AppProps) => {
    // @ts-ignore
    const Layout = Component.Layout || DefaultLayout;

    return (
        <Layout>
            <Component {...pageProps}/>
        </Layout>
    );
};

const App = compose<AppProps, AppProps>(
    withMobileObserver,
    lifecycle({
        componentDidMount() {
            handlePopupObservers();
        },
    }),
)((props) => (
    <InnerApp {...props}/>
));

export default (props) => (
    <Provider store={store}>
        <App {...props}/>
    </Provider>
);
