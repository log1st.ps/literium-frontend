import dynamic from "next/dynamic";
import Link from "next/link";
import {NextRouter, useRouter} from "next/router";
import React, {ReactNode} from "react";
import {compose, lifecycle, withHandlers, withProps, withState} from "recompose";
import {withMobileObserver} from "../../src/hoc/withMobileObserver";
import {withTranslation} from "../../src/hoc/withTranslation";
import {ITranslation} from "../../src/interfaces/ITranslation";
import {ButtonsList} from "../../src/templates/components/buttonsList/ButtonsList";
import {Card} from "../../src/templates/components/card/Card";
import {Divider} from "../../src/templates/components/divider/Divider";
import {Field} from "../../src/templates/components/form/elements/field/Field";
import {Form} from "../../src/templates/components/form/Form";
import {Img} from "../../src/templates/components/image/Img";
import {H5, P} from "../../src/templates/components/typography/Typography";
import {Icon} from "../../src/templates/elements/icon/Icon";
import {Col, Grid, Margin, Row} from "../../src/templates/layouts/grid/Grid";

const Modal = dynamic(
    () => import("../../src/templates/components/modal/Modal")
        .then((file) => file.Modal,
    ), {ssr: false});

interface ITransactionPage {
    texts: ITranslation["pages"]["transaction"];

    isPasswordModalActive: boolean;
    showPasswordModal(): void;
    closePasswordModal(): void;
    setIsPasswordModalActive(isPasswordModalActive: boolean): void;

    isFeeModalActive: boolean;
    showFeeModal(): void;
    closeFeeModal(): void;
    setIsFeeModalActive(isFeeModalActive: boolean): void;

    isMobile: boolean;

    password?: string;
    setPassword?(password: string): void;

    router: NextRouter;

    link: string;
    from: number;
    to: number;

    children?: ReactNode;
}

const Inner = ({
    isMobile,
    isPasswordModalActive,
    closePasswordModal,
    isFeeModalActive,
    closeFeeModal,
    texts,
    password,
    setPassword,
    link,
    from,
    to,
}: ITransactionPage) => (
    <div>
        Transaction page
        <Modal
            width={550}
            onClose={closePasswordModal}
            isActive={isPasswordModalActive}
        >
            <Margin top={isMobile ? 0 : -2}>
                <Card isMobile={isMobile}>
                    <Margin top={isMobile ? 40 : 20}>
                        <Row justify={"center"}>
                            <Img
                                url={require("../../src/assets/images/lock.png")}
                                height={130}
                                width={130}
                                borderRadius={65}
                                type={"block"}
                            />
                        </Row>
                    </Margin>
                    <Margin top={24}>
                        <H5 isMedium={true} align={"center"} color={"dark"}>{texts.confirmModal.title}</H5>
                    </Margin>
                    <Margin top={8}>
                        <P color={"grey"} align={"center"}>
                            {
                                isMobile
                                    ? texts.confirmModal.description.replace(/\n/g, " ")
                                    : texts.confirmModal.description
                                        .split("\n").map((i, index) => (<div key={index}>{i}</div>))
                            }
                        </P>
                    </Margin>
                    <Margin top={28}>
                        <Form.Input
                            type={"password"}
                            value={password}
                            onInput={setPassword}
                            placeholder={texts.confirmModal.password}
                            height={67}
                        />
                    </Margin>
                    <Margin top={isMobile ? 30 : 24}>
                        <ButtonsList
                            gap={28}
                            direction={isMobile ? "columnReverse" : "row"}
                            justify={"end"}
                            buttons={[
                                {
                                    key: "cancel",
                                    state: "secondaryPure",
                                    label: texts.confirmModal.cancel,
                                    size: "medium",
                                    isBlock: isMobile,
                                    onClick: closePasswordModal,
                                },
                                {
                                    key: "confirm",
                                    state: "secondary",
                                    label: texts.confirmModal.confirm,
                                    size: "medium",
                                    isBlock: isMobile,
                                },
                            ]}
                        />
                    </Margin>
                </Card>
            </Margin>
        </Modal>
        <Modal
            width={550}
            onClose={closeFeeModal}
            isActive={isFeeModalActive}
        >
            <Margin top={isMobile ? -6 : 0}>
                <Card isMobile={isMobile}>
                    `<Margin top={isMobile ? 40 : 20}>
                        <Row justify={"center"}>
                            <Img
                                url={require(`../../src/assets/images/fee${isMobile ? "Mobile" : ""}.png`)}
                                height={138}
                                width={138}
                                type={"block"}
                                backgroundType={"contain"}
                            />
                        </Row>
                    </Margin>
                    <Margin top={16}>
                        <H5 isMedium={true} align={"center"} color={"dark"}>{texts.feeModal.title}</H5>
                    </Margin>
                    <Margin top={8}>
                        <P color={"grey"} align={"center"}>
                            {
                                isMobile
                                    ? texts.feeModal.description.replace(/\n/g, " ")
                                    : texts.feeModal.description
                                        .split("\n").map((i, index) => (<div key={index}>{i}</div>))
                            }
                        </P>
                    </Margin>`
                    <Margin top={isMobile ? 28 : 40}>
                        <Grid gap={isMobile ? 27 : 40}>
                            <Field label={texts.feeModal.link}>
                                <Margin top={-4}>
                                    <Row gap={17} align={"center"}>
                                        <Col gap={17}>
                                            <H5 color={"dark"} isMedium={true}>#{link}</H5>
                                        </Col>
                                        <Col gap={17} isShrink={false}>
                                            <Margin top={-2}>
                                                <Link href={`#${link}`}>
                                                    <a target={"_blank"}>
                                                        <Icon type={"link"} size={"sm"}/>
                                                    </a>
                                                </Link>
                                            </Margin>
                                        </Col>
                                    </Row>
                                </Margin>
                            </Field>
                            <Grid
                                cols={isMobile ? 1 : ["auto", "40px", "auto"]}
                                gap={isMobile ? 30 : 12}
                                align={"center"}
                            >
                                <Field label={texts.feeModal.from}>
                                    <Margin top={-4}>
                                        <H5
                                            color={"dark"}
                                            isMedium={true}
                                        >
                                            {texts.feeModal.value.messageFormat({value: from, currency: "BTC"})}
                                        </H5>
                                    </Margin>
                                </Field>
                                {!isMobile && (
                                    <Divider isDark={true}/>
                                )}
                                <Field label={texts.feeModal.to}>
                                    <Margin top={-4}>
                                        <H5
                                            color={"dark"}
                                            isMedium={true}
                                        >
                                            {texts.feeModal.value.messageFormat({value: to, currency: "BTC"})}
                                        </H5>
                                    </Margin>
                                </Field>
                            </Grid>
                        </Grid>
                    </Margin>
                    <Divider isDark={true} margin={isMobile ? 27 : 40}/>
                    <Margin top={isMobile ? 20 : 20}>
                        <ButtonsList
                            gap={28}
                            direction={isMobile ? "columnReverse" : "row"}
                            justify={"end"}
                            buttons={[
                                {
                                    key: "done",
                                    state: "secondary",
                                    label: texts.feeModal.done,
                                    size: "medium",
                                    isBlock: isMobile,
                                },
                            ]}
                        />
                    </Margin>
                </Card>
            </Margin>
        </Modal>
    </div>
);

const Page = compose(
    withTranslation("pages.transaction"),
    withMobileObserver,
    withProps(() => ({
        router: useRouter(),
    })),

    withState("password", "setPassword", ""),
    withState("isPasswordModalActive", "setIsPasswordModalActive", false),
    withState("isFeeModalActive", "setIsFeeModalActive", false),

    withProps({
        link: "938737293947293",
        from: 999.9999999,
        to: 0.00124411,
    }),

    withHandlers({
        closePasswordModal: ({setIsPasswordModalActive}) => () => {
            setIsPasswordModalActive(false);
        },
        showPasswordModal: ({setIsPasswordModalActive}) => () => {
            setIsPasswordModalActive(true);
        },
        closeFeeModal: ({setIsFeeModalActive}) => () => {
            setIsFeeModalActive(false);
        },
        showFeeModal: ({setIsFeeModalActive}) => () => {
            setIsFeeModalActive(true);
        },
    }),

    lifecycle({
        componentDidMount() {
            setTimeout(() => {
                const {router, setIsFeeModalActive, setIsPasswordModalActive} = this.props as ITransactionPage;

                if (!!router.query.withConfirmModal) {
                    setIsPasswordModalActive(true);
                }

                if (!!router.query.withFeeModal) {
                    setIsFeeModalActive(true);
                }

            });
        },
    }),
)(Inner);

export default Page;
