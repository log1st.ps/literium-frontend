import {NextRouter, useRouter} from "next/router";
import React from "react";
import {compose, lifecycle, withHandlers, withProps, withState} from "recompose";
import {EmptyListContainer} from "../src/containers/EmptyListContainer";
import {ListingContainer} from "../src/containers/ListingContainer";
import {withMobileObserver} from "../src/hoc/withMobileObserver";
import {withTranslation} from "../src/hoc/withTranslation";
import {ITranslation} from "../src/interfaces/ITranslation";
import {withPageTitle} from "../src/hoc/withPageTitle";

interface IInvoicesPage {
    isEmpty: boolean;
    setIsEmpty(isEmpty: boolean): void;
    setEmpty(): void;
    setNotEmpty(): void;

    isMobile: boolean;

    router: NextRouter;

    texts: ITranslation["pages"]["addresses"];
}

const Inner = ({
    texts,
    isMobile,
    isEmpty,
}: IInvoicesPage) => (
    <div>
        {isEmpty ? (
            <EmptyListContainer
                isMobile={isMobile}
                image={require(`../src/assets/images/noAddresses${isMobile ? "Mobile" : ""}.png`)}
                title={texts.empty.title}
                subTitle={texts.empty.subTitle}
                buttonLabel={texts.empty.create}
                buttonIcon={"plus"}
                imgLeft={isMobile ? 2 : 4}
            />
        ) : (
            <ListingContainer
                isMobile={isMobile}
                modelName={texts.title}
            />
        )}
    </div>
);

const Page = compose(
    withTranslation("pages.addresses"),
    withPageTitle((texts) => texts.pages.addresses.title),
    withMobileObserver,
    withState("isEmpty", "setIsEmpty", true),
    withHandlers({
        setEmpty: ({setIsEmpty}) => () => {
            setIsEmpty(true);
        },
        setNotEmpty: ({setIsEmpty}) => () => {
            setIsEmpty(false);
        },
    }),
    withProps(() => ({
        router: useRouter(),
    })),
    lifecycle({
        componentDidMount() {
            setTimeout(() => {
                const {setNotEmpty, router} = this.props as IInvoicesPage;

                if (!!router.query.isNotEmpty) {
                    setNotEmpty();
                }
            });
        },
    }),
)(Inner as any);

export default Page;
