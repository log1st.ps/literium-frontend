import {Storybook} from "../storybook/Storybook";

// @ts-ignore
Storybook.Layout = ({children}) => (children);

export default Storybook;
