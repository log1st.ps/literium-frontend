import {useRouter} from "next/router";
import React, {Fragment} from "react";
import {compose, withProps} from "recompose";
import {EmptyListContainer} from "../src/containers/EmptyListContainer";
import {ListingContainer} from "../src/containers/ListingContainer";
import {withMobileObserver} from "../src/hoc/withMobileObserver";
import {withPageTitle} from "../src/hoc/withPageTitle";
import {withTranslation} from "../src/hoc/withTranslation";
import {ITranslation} from "../src/interfaces/ITranslation";

interface IPage {
    isEmpty: boolean;
    texts: ITranslation["pages"]["transactions"];
    isMobile: boolean;
}

const Inner = ({
    isEmpty,
    texts,
    isMobile,
}: IPage) => (
    <Fragment>
        {isEmpty ? (
            <EmptyListContainer
                isMobile={isMobile}
                image={require(`../src/assets/images/noTransactions${isMobile ? "Mobile" : ""}.png`)}
                title={texts.empty.title}
                subTitle={texts.empty.subTitle}
                buttonLabel={texts.empty.create}
                buttonIcon={"plus"}
                imgLeft={2}
            />
        ) : (
            <ListingContainer
                isMobile={isMobile}
                modelName={texts.title}
                sortAttributes={{
                    date: {
                        label: texts.sort.date,
                        sorts: ["asc", "desc"],
                        icon: "date",
                    },
                    amount: {
                        label: texts.sort.amount,
                        sorts: ["asc", "desc"],
                        icon: "plusMinus",
                    },
                    unconfirmed: {
                        label: texts.sort.unconfirmed,
                        isCheckbox: true,
                        icon: "question",
                    },
                }}
            />
        )}
    </Fragment>
);

const Page = compose<any, IPage>(
    withMobileObserver,
    withTranslation("pages.transactions"),
    withPageTitle((texts) => texts.pages.transactions.title),
    withProps(() => ({
        router: useRouter(),
    })),
    withProps(({router}) => ({
        isEmpty: !router.query.isNotEmpty,
    })),
)(Inner);

export default Page;
