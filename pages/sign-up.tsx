import Link from "next/link";
import {useRouter} from "next/router";
import React from "react";
import {compose, withProps} from "recompose";
import {BlockedContainer} from "../src/containers/signUp/BlockedContainer";
import {ImportWalletContainer} from "../src/containers/signUp/ImportWalletContainer";
import {ImportWalletLastContainer} from "../src/containers/signUp/ImportWalletLastContainer";
import {LoginContainer} from "../src/containers/signUp/LoginContainer";
import {PasswordCreationContainer} from "../src/containers/signUp/PasswordCreationContainer";
import {PhraseGenerationContainer} from "../src/containers/signUp/PhraseGenerationContainer";
import {WelcomeBackContainer} from "../src/containers/signUp/WelcomeBackContainer";
import {getLandingAndLoadersProps, ISignUpPage} from "../src/containers/signUpFlow";
import {withMobileObserver} from "../src/hoc/withMobileObserver";
import {withTranslation} from "../src/hoc/withTranslation";
import {Logo} from "../src/templates/components/logo/Logo";
import {H3, H5, H6, P} from "../src/templates/components/typography/Typography";
import {FullHeightEmptyLayout} from "../src/templates/layouts/fullHeightEmpty/FullHeightEmptyLayout";
import {
    availableSignUpForms,
    availableSignUpSteps,
    SignUpLayout,
} from "../src/templates/layouts/signUp/base/SignUpLayout";

const SignUpPage = ({
    isForm,
    FormContainer,
    ...props
}: ISignUpPage) => (
    <SignUpLayout
        isMobile={props.isMobile}
        renderLogo={(
            <Link href={"/"}>
                <a>
                    <Logo/>
                </a>
            </Link>
        )}
        isForm={isForm}
        {...(!isForm ? getLandingAndLoadersProps(props) : {})}
    >
        {isForm && (
            <div id={"signUp"}>
                <FormContainer/>
            </div>
        )}
    </SignUpLayout>
);

const Page = compose(
    withTranslation("pages.signUp"),
    withMobileObserver,
    withProps(({isMobile}) => ({
        TitleComponent: isMobile ? H5 : H3,
        HintComponent: isMobile ? H6 : H5,
    })),
    withProps(({router}) => {
        const step = String(useRouter().query.step || "initial") as any;

        return {
            step: [...availableSignUpSteps, ...availableSignUpForms].indexOf(step) > -1 ? step : "initial",
            hint: ["loginSuccess", "created" , "imported"].indexOf(step) > -1 && "Redirecting to Your dashboard...",
            isForm: availableSignUpForms.indexOf(step) > -1,
        };
    }),
    withProps(({isForm, step}) => ({
        FormContainer: isForm && {
            phraseGeneration: PhraseGenerationContainer,
            passwordCreation: PasswordCreationContainer,
            welcomeBack: WelcomeBackContainer,
            login: LoginContainer,
            blocked: BlockedContainer,
            importWallet: ImportWalletContainer,
            importWalletLast: ImportWalletLastContainer,
        }[step],
    })),
)(SignUpPage);

// @ts-ignore
Page.Layout = FullHeightEmptyLayout;

export default Page;
