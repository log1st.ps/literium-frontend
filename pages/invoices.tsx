import dynamic from "next/dynamic";
import {NextRouter, useRouter} from "next/router";
import React, { Fragment } from "react";
import {compose, lifecycle, withHandlers, withProps, withState} from "recompose";
import {EmptyListContainer} from "../src/containers/EmptyListContainer";
import {ListingContainer} from "../src/containers/ListingContainer";
import {withMobileObserver} from "../src/hoc/withMobileObserver";
import {withPageTitle} from "../src/hoc/withPageTitle";
import {withTranslation} from "../src/hoc/withTranslation";
import {ITranslation} from "../src/interfaces/ITranslation";
import {ButtonsList} from "../src/templates/components/buttonsList/ButtonsList";
import {Card} from "../src/templates/components/card/Card";
import {Img} from "../src/templates/components/image/Img";
import {ITableData} from "../src/templates/components/table/Table";
import {H5, P, S} from "../src/templates/components/typography/Typography";
import {Icon} from "../src/templates/elements/icon/Icon";
import {Col, Margin, Row} from "../src/templates/layouts/grid/Grid";

const Modal = dynamic(
    () => import("../src/templates/components/modal/Modal")
        .then((file) => file.Modal,
        ), {ssr: false});

interface IInvoicesPage {
    isDeleteModalActive: boolean;
    setIsDeleteModalActive(isDeleteModalActive: boolean): void;
    showDeleteModal(): void;
    hideDeleteModal(): void;

    isEmpty: boolean;
    setIsEmpty(isEmpty: boolean): void;
    setEmpty(): void;
    setNotEmpty(): void;

    isMobile: boolean;

    router: NextRouter;

    texts: ITranslation["pages"]["invoices"];
    invoicesTexts: ITranslation["invoices"];

    onRowClick?(row: ITableData): () => void;
}

const Inner = ({
    isDeleteModalActive,
    hideDeleteModal,
    texts,
    isMobile,
    isEmpty,
    invoicesTexts,
    onRowClick,
}: IInvoicesPage) => (
    <div>
        {isEmpty ? (
            <EmptyListContainer
                isMobile={isMobile}
                image={require(`../src/assets/images/noInvoices${isMobile ? "Mobile" : ""}.png`)}
                title={texts.empty.title}
                subTitle={texts.empty.subTitle}
                buttonLabel={texts.empty.create}
                buttonIcon={"plus"}
                imgLeft={isMobile ? 20 : 26}
            />
        ) : (
            <ListingContainer
                onRowClick={onRowClick}
                isMobile={isMobile}
                modelName={texts.title}
                primaryActions={[
                    {
                        key: "create",
                        state: "default",
                        size: "default",
                        renderBefore: (
                            <Icon type={"plus"} size={"lt"}/>
                        ),
                        label: texts.create,
                    },
                ]}
                sortAttributes={{
                    date: {
                        label: texts.sort.date,
                        sorts: ["asc", "desc"],
                        icon: "date",
                    },
                    amount: {
                        label: texts.sort.amount,
                        sorts: ["asc", "desc"],
                        icon: "plusMinus",
                    },
                    unconfirmed: {
                        label: texts.sort.pending,
                        isCheckbox: true,
                        icon: "question",
                    },
                }}
                filterConfig={{
                    date: {
                        type: "datepicker",
                        label: texts.filter.date.label,
                        icon: "date",
                        value: null,
                    },
                    // status: {
                    //     type: "che",
                    //     label: texts.filter.status.label,
                    //     icon: "commentCheck",
                    //     value: [],
                    //     config: {
                    //         options: [
//                                {{key: "paid", text: texts.filter.status.paid},}
                    //                              {{key: "pending", text: texts.filter.status.pending},}
                    // ],
                    // },
                    // },
                    // amount: {
                    //     label: texts.filter.amount.label,
                    //     icon: "plusMinus",
                    //     value: [0, 1000],
                    //     config: {
                    //         min: 0,
                    //         max: 1000,
                    //         valueLabel: texts.filter.amount.value,
                    //     },
                    // },
                }}
                columns={isMobile ? [
                    "name",
                    "status",
                    "amount",
                ] : [
                    {
                        key: "date",
                        label: texts.table.date,
                    },
                    {
                        key: "name",
                        label: texts.table.name,
                    },
                    {
                        key: "amount",
                        label: texts.table.amount,
                    },
                    {
                        key: "status",
                        label: texts.table.status,
                    },
                ]}
                data={Array(isMobile ? 3 : 6).fill({
                    raw: {
                        date: "Mon, March 1 2019",
                        name: {
                            hint: "Invoice 001 - Design Project",
                        },
                        amount: isMobile ? {
                            hint: "999.99999999 BTC",
                        } : {
                            value: "999.99999999 BTC",
                            hint: "IDR 345.870.000",
                        },
                        status: "paid",
                    },
                }).map(({raw}, index) => ({raw: {...raw, id: index + 1}, index}))}
                rowActions={[
                    {
                        key: "delete",
                        label: texts.table.actions.delete,
                        handler(row: ITableData) {
                            return () => {
                                console.log(row);
                            };
                        },
                        color: "red",
                        icon: "trash",
                    },
                ]}
                renderRowCellInner={{
                    status: (column, row) => (
                        <Fragment>
                            <Row align={"center"} gap={12}>
                                <Col gap={12}>
                                    <Icon type={"oval"} size={"lt"}/>
                                </Col>
                                <Col gap={12}>
                                    <P color={"green"}>
                                        {invoicesTexts.statuses[row.raw.status.value]}
                                    </P>
                                </Col>
                            </Row>
                        </Fragment>
                    ),
                }}
            />
        )}
        <Modal
            width={550}
            onClose={hideDeleteModal}
            isActive={isDeleteModalActive}
        >
            <Margin top={0}>
                <Card isMobile={isMobile}>
                    <Margin top={isMobile ? 40 : 20}>
                        <Row justify={"center"}>
                            <Img
                                url={require(`../src/assets/images/delete.png`)}
                                height={130}
                                width={130}
                                type={"block"}
                                backgroundType={"contain"}
                            />
                        </Row>
                    </Margin>
                    <Margin top={24}>
                        <H5 isMedium={true} align={"center"} color={"dark"}>{texts.deleteModal.title}</H5>
                    </Margin>
                    <Margin top={8}>
                        <P color={"grey"} align={"center"}>
                            {
                                isMobile
                                    ? texts.deleteModal.subTitle.replace(/\n/g, " ")
                                    : texts.deleteModal.subTitle
                                        .split("\n").map((i, index) => (<div key={index}>{i}</div>))
                            }
                        </P>
                    </Margin>
                    <Margin top={isMobile ? 28 : 40}>
                        <ButtonsList
                            gap={28}
                            direction={isMobile ? "columnReverse" : "row"}
                            justify={"end"}
                            buttons={[
                                {
                                    key: "cancel",
                                    state: "secondaryPure",
                                    label: texts.deleteModal.cancel,
                                    size: "medium",
                                    isBlock: isMobile,
                                    onClick: hideDeleteModal,
                                },
                                {
                                    key: "confirm",
                                    state: "secondary",
                                    label: texts.deleteModal.confirm,
                                    size: "medium",
                                    isBlock: isMobile,
                                },
                            ]}
                        />
                    </Margin>
                </Card>
            </Margin>
        </Modal>
    </div>
);

const Page = compose(
    withTranslation("pages.invoices"),
    withTranslation("invoices", "invoicesTexts"),
    withPageTitle((texts) => texts.pages.invoices.title),
    withMobileObserver,
    withState("isDeleteModalActive", "setIsDeleteModalActive", false),
    withState("isEmpty", "setIsEmpty", true),
    withProps(() => ({
        router: useRouter(),
    })),
    withHandlers({
        onRowClick: ({router}) => (row) => () => {
            router.push(`/invoices/${row.raw.id.value}`);
        },
        showDeleteModal: ({setIsDeleteModalActive}) => () => {
            setIsDeleteModalActive(true);
        },
        hideDeleteModal: ({setIsDeleteModalActive}) => () => {
            setIsDeleteModalActive(false);
        },
        setEmpty: ({setIsEmpty}) => () => {
            setIsEmpty(true);
        },
        setNotEmpty: ({setIsEmpty}) => () => {
            setIsEmpty(false);
        },
    }),
    lifecycle({
        componentDidMount() {
            setTimeout(() => {
                const {showDeleteModal, setNotEmpty, router} = this.props as IInvoicesPage;

                if (!!router.query.withDeleteModal) {
                    showDeleteModal();
                }

                if (!!router.query.isNotEmpty) {
                    setNotEmpty();
                }
            });
        },
    }),
)(Inner as any);

export default Page;
