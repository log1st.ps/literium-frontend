import {withProps} from "recompose";
import {Storybook} from "../../storybook/Storybook";

const Page = withProps({
    storyOnly: true,
})(Storybook);

// @ts-ignore
Page.Layout = ({children}) => (children);

export default Page;
