import React from "react";
import {compose} from "recompose";
import {withPageTitle} from "../src/hoc/withPageTitle";

const IndexPage = ({

}) => (
    <div>
        Index
    </div>
);

export default compose<any, any>(
    withPageTitle((texts) => texts.pages.index.previous),
)(IndexPage);
