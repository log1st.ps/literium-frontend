import {NextRouter, useRouter} from "next/router";
import {compose, lifecycle, withProps, withState} from "recompose";

interface IInvoicePage {
    router?: NextRouter;
    id?: number;
    setId?(id: number): void;

    children?: any;
}

const Inner = ({
    id,
}: IInvoicePage) => (
    <div>
        Invoice #{id}
    </div>
);

const Page = compose(
    withProps(() => ({
        router: useRouter(),
    })),
    withState("id", "setId", null),
    lifecycle<IInvoicePage, any>({
        componentDidMount() {

            setTimeout(() => {
                const {setId, router} = this.props;
                setId(+router.query.id);
            });
        },
    }),
)(Inner);

export default Page;
