/* tslint:disable:max-line-length */
import Document, { Head, Html, Main, NextScript } from "next/document";
import React from "react";

class MyDocument extends Document {

    public render() {
        return (
            <Html>
                <Head>
                    <link
                        href={"https://fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&display=swap"}
                        rel={"stylesheet"}
                    />
                    <link rel="manifest" href="/static/manifest.json" />
                </Head>
                <body>
                <Main />
                <NextScript />
                </body>
            </Html>
        );
    }
}

export default MyDocument;

