import {ITranslation} from "../interfaces/ITranslation";

export default {
    side: {
        dashboard: "Dashboard",
        transactions: "Transactions",
        invoices: "My Invoices",
        addresses: "My Addresses",
        settings: "Settings",
        help: "Help",
        signOut: "Sign Out",
    },
    pages: {
        signUp: {
            signIn: "Sign In",
            title: "Signup now and experience the easy way\nto send cryptocurrency",
            createWallet: "Create My Wallet",
            importWallet: "Import Wallet",
            step: {
                encrypting: "Please wait, We do some magic encrypting Your Password...",
                import: "Please wait,\nWe are preparing your wallet...",
                loginSuccess: "Login Success",
                created: "Congratulations!,\nYou already created Your New Wallet ",
                imported: "Congratulations!, You already\nimported Your New Wallet ",
            },
            form: {
                phraseGeneration: {
                    title: "Here is your\nphrase password:",
                    copy: "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                    copyButton: "COPY",
                    alert: "IMPORTANT : You need to saved your phrase password to login to your account. Saved on your note or somewhere now",
                    agreement: "It's okay, I already saved the phrase password",
                    back: "Back to Home",
                    continue: "Continue",
                },
                passwordCreation: {
                    title: "Let's encrypt Your\npassword",
                    subTitle: "Type your password to start process encryption password",
                    password: "Type your phrase password",
                    passwordConfirmation: "Re-type your password",
                    hint: "The estimated time to crack your password using the power of all computers on Earth is about 1 billion years.",
                    skip: "Skip This Step",
                    continue: "Next",
                    back: "Back to Home",
                    skipModal: {
                        title: "Are you sure to skip this step?",
                        description: "It's not recommended to skip this step. We recommended you to encrypt your password to avoid someone who try breaking your password",
                        cancel: "Cancel",
                        confirm: "Yes, Skip this step",
                    },
                },
                welcomeBack: {
                    title: "Hi, {name}",
                    subTitle: "One step again to access your account",
                    password: "Type your password",
                    errors: {
                        incorrect: "Incorrect Address, Please try again",
                    },
                    switch: "Switch Wallet",
                    continue: "Continue",
                },
                login: {
                    title: "Welcome Back",
                    subTitle: "To login to Your account, type your phrase password",
                    password: "Type your phrase password",
                    back: "Back to Create Wallet",
                    continue: "Continue",
                },
                blocked: {
                    title: "Account Temporary Blocked",
                    subTitle: "Hi {name}, You are already enters the wrong password more than 3 times. You can try to input your password in 15 minutes. Please wait...",
                    hours: "{n, plural, =1{Hour} other{Hours}}",
                    minutes: "{n, plural, =1{Minute} other{Minutes}}",
                    seconds: "{n, plural, =1{Second} other{Seconds}}",
                    back: "Back to Home",
                },
                importWallet: {
                    title: "Import Your Wallet",
                    subTitle: "To import your wallet, type your password below",
                    password: "Type your phrase password",
                    back: "Back to Create Wallet",
                    continue: "Continue",
                },
                importWalletLast: {
                    title: "Last step to import Your Wallet",
                    subTitle: "Please type your password encryption",
                    password: "Type your phrase password",
                    back: "Back to Create Wallet",
                    continue: "Continue",
                },
            },
        },
        index: {
            previous: "Dashboard",
        },
        transactions: {
            previous: "Transactions",
            title: "Transactions",
            empty: {
                title: "You don’t have transactions",
                subTitle: "lorem Ipsum is simply dummy text of the printing and typesetting\nindustrylorem Ipsum is simply dummy text of the printing and\ntypesetting industrylorem Ipsum is simply dummy text of ",
                create: "Create New Transactions",
            },
            sort: {
                date: "Date",
                amount: "Amount",
                unconfirmed: "Unconfirmed first",
            },
        },
        transaction: {
            confirmModal: {
                title: "Confirm Password",
                description: "Please type your password to continue send coins",
                password: "Enter your password",
                cancel: "Cancel",
                confirm: "Send Coins",
            },
            feeModal: {
                title: "Success Change Fee",
                description: "You are already change the fee",
                done: "Done",
                link: "Link",
                from: "From",
                to: "To",
                value: "{value} {currency}",
            },
        },
        invoices: {
            title: "My Invoices",
            create: "Create New Invoice",
            deleteModal: {
                title: "Are you sure want to delete?",
                subTitle: "When you are delete the invoice the action can\nnot be undo. Are you sure?",
                cancel: "Cancel",
                confirm: "Yes, Delete Invoice",
            },
            empty: {
                title: "You don’t have Invoices",
                subTitle: "lorem Ipsum is simply dummy text of the printing and typesetting\nindustrylorem Ipsum is simply dummy text of the printing and\ntypesetting industrylorem Ipsum is simply dummy text of ",
                create: "Create New Invoice",
            },
            sort: {
                date: "Date",
                amount: "Amount",
                pending: "Pending first",
            },
            filter: {
                date: {
                    label: "Date",
                },
                status: {
                    label: "Status",
                    paid: "Paid",
                    pending: "Pending",
                },
                amount: {
                    label: "Amount",
                    value: "{n} BTC",
                },
            },
            table: {
                date: "Date of creation",
                name: "Name",
                amount: "Amount",
                status: "Status",
                actions: {
                    delete: "Delete",
                },
            },
        },
        addresses: {
            title: "My Addresses",
            empty: {
                title: "You don’t have Address",
                subTitle: "lorem Ipsum is simply dummy text of the printing and typesetting\nindustrylorem Ipsum is simply dummy text of the printing and\ntypesetting industrylorem Ipsum is simply dummy text of ",
                create: "Create New Address",
            },
        },
    },
    header: {
        back: "Back to {page}",
        count: "Всего {n, plural, =0{ни одной записи} =1{одна запись} few{# записи} many{# записей}}",
    },
    listing: {
        sortBy: "Sort by",
        filterBy: "Filter by",
        search: "Search {model}",
        sort: {
            asc: "Ascending (A-Z)",
            desc: "Descending (Z-A)",
        },
        filter: {
            reset: "Reset",
            apply: "Apply",
        },
        load: "Load More",
    },
    invoices: {
        statuses: {
            paid: "Paid",
        },
    },
} as ITranslation;
