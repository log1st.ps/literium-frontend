import Axios, {AxiosRequestConfig} from "axios";

export const getRequest = Axios.get;
export const postRequest = Axios.post;
export const putRequest = Axios.put;
export const deleteRequest = Axios.delete;
export const optionsRequest = (url: string, config: AxiosRequestConfig) => Axios.create({
    url,
    ...config,
    method: "options",
});
