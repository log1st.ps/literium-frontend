export interface IUser {
    avatarUrl?: string;
    firstName?: string;
}
