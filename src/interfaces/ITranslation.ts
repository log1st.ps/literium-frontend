export interface ITranslation {
    side: {
        dashboard;
        transactions;
        invoices;
        addresses;
        settings;
        help;
        signOut;
    };
    pages: {
        index: {
            previous;
        };
        signUp: {
            signIn;
            title;
            createWallet;
            importWallet;
            step: {
                encrypting;
                import;
                loginSuccess;
                created;
                imported;
            };
            form: {
                phraseGeneration: {
                    title;
                    copy;
                    copyButton;
                    alert;
                    agreement;
                    back;
                    continue;
                },
                passwordCreation: {
                    title;
                    subTitle;
                    password;
                    passwordConfirmation;
                    hint;
                    skip;
                    continue;
                    back;
                    skipModal: {
                        title;
                        description;
                        cancel;
                        confirm;
                    }
                },
                welcomeBack: {
                    title;
                    subTitle;
                    password;
                    errors: {
                        incorrect;
                    },
                    switch;
                    continue;
                },
                login: {
                    title;
                    subTitle;
                    password;
                    back;
                    continue;
                },
                blocked: {
                    title;
                    subTitle;
                    hours;
                    minutes;
                    seconds;
                    back;
                },
                importWallet: {
                    title;
                    subTitle;
                    password;
                    back;
                    continue;
                },
                importWalletLast: {
                    title;
                    subTitle;
                    password;
                    back;
                    continue;
                },
            }
        };
        transactions: {
            previous;
            title;
            empty: {
                title;
                subTitle;
                create;
            };
            sort: {
                date;
                amount;
                unconfirmed;
            }
        };
        transaction: {
            confirmModal: {
                title;
                description;
                password;
                cancel;
                confirm;
            },
            feeModal: {
                title;
                description;
                done;
                link;
                from;
                to;
                value;
            },
        }
        invoices: {
            title;
            create;
            deleteModal: {
                title;
                subTitle;
                cancel;
                confirm;
            },
            empty: {
                title;
                subTitle;
                create;
            },
            sort: {
                date;
                amount;
                pending;
            }
            filter: {
                date: {
                    label;
                },
                status: {
                    label;
                    paid;
                    pending;
                },
                amount: {
                    label;
                    value;
                },
            }
            table: {
                date;
                name;
                amount;
                status;
                actions: {
                    delete;
                },
            }
        }
        addresses: {
            title;
            empty: {
                title;
                subTitle;
                create;
            },
        }
    };
    header: {
        back;
    };
    listing: {
        sortBy;
        filterBy;
        search;
        sort: {
            [key: string]: string;
        };
        filter: {
            reset;
            apply;
        };
        load;
    };
    invoices: {
        statuses: {
            [key: string]: string;
        },
    };
}
