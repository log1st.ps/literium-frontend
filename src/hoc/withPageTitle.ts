import {connect} from "react-redux";
import {compose, lifecycle} from "recompose";
import {ITranslation} from "../interfaces/ITranslation";
import {setPageTitle} from "../store/global/actions";
import {withTranslation} from "./withTranslation";

export const withPageTitle = (title: string | ((texts: ITranslation) => string)) => compose(
    connect(
        null,
        {
            setTitle: setPageTitle,
        },
    ),
    withTranslation(null, "pageTitleTexts"),
    lifecycle({
        componentDidMount() {
            const {
                setTitle,
                pageTitleTexts,
            } = this.props as any;

            setTitle(typeof title === "string" ? title : title(pageTitleTexts));
        },
    }),
);
