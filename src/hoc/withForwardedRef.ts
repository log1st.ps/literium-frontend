import {ReactComponentLike} from "prop-types";
import {
    createElement,
    forwardRef,
} from "react";

const withForwardedRef
    = <T>(refName: string = "forwardedRef") => (base: ReactComponentLike) =>
    forwardRef<any, T>((props: T, ref) => createElement(
        base,
        {
            ...props,
            [refName]: ref,
        },
    ));

export {
    withForwardedRef,
};
