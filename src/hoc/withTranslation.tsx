import React, {ReactNode} from "react";
import {connect} from "react-redux";
import {ITranslation} from "../interfaces/ITranslation";
import {IState} from "../store/store";

type translationTarget = string | {[key: string]: any};

const dotFind = (target, object) => {
    return target.split(".").reduce((p, c) => p && p[c] || null, object);
};

const stateToTexts = (target?: translationTarget) => (state: ITranslation) => {
    return target ? (
        typeof target === "string"
            ? dotFind(target, state)
            : Object.entries(target).reduce((previous, [key, t]) => ({
                ...previous,
                [key]: dotFind(t, state),
            }), {})
    ) : state;
};

export const withTranslation = (
    target?: translationTarget,
    propName = "texts",
) => (Component: ReactNode | any) => connect(
    (state: IState) => ({
        [propName]: stateToTexts(target)(state.i18n.translation),
    }),
)(Component);
