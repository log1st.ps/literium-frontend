import {connect} from "react-redux";
import {compose, lifecycle, withState} from "recompose";
import {setIsMobile} from "../store/global/actions";
import {IState} from "../store/store";

const handleResize = (updateIsMobile: ((value: boolean) => void)) => function() {
    requestAnimationFrame(() => {
        const width = window.innerWidth;

        const {
            isMobile,
        } = this.props;

        const computed = width <= 360;
        if (isMobile !== computed) {
            updateIsMobile(computed);
        }
    });
};

const withMobileObserver = compose(
    withState("listener", "setListener", null),
    connect(
        (state: IState) => ({
            isMobile: state.global.isMobile,
        }),
        {
            updateIsMobile: setIsMobile,
        },
    ),
    lifecycle({
        componentDidMount() {
            const {
                updateIsMobile,
                setListener,
            } = this.props as any;

            const listener = handleResize(updateIsMobile).bind(this);
            setListener(listener);

            window.addEventListener("resize", listener);
        },
        componentWillUnmount() {
            const {
                listener,
            } = this.props as any;
            window.removeEventListener("resize", listener);
        },
    }),
);

export {
    withMobileObserver,
};
