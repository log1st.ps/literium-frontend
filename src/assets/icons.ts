import AngleBottomIcon from "../templates/elements/icon/assets/angleBottom.inline.svg";
import AngleLeftIcon from "../templates/elements/icon/assets/angleLeft.inline.svg";
import FileCSVIcon from "../templates/elements/icon/assets/fileCSV.inline.svg";
import FileJSONIcon from "../templates/elements/icon/assets/fileJSON.inline.svg";
import FilesIcon from "../templates/elements/icon/assets/files.inline.svg";
import FileXMLIcon from "../templates/elements/icon/assets/fileXML.inline.svg";
import GearIcon from "../templates/elements/icon/assets/gear.inline.svg";
import HamburgerIcon from "../templates/elements/icon/assets/hamburger.inline.svg";
import HomeIcon from "../templates/elements/icon/assets/home.inline.svg";
import LifebuoyIcon from "../templates/elements/icon/assets/lifebuoy.inline.svg";
import NotificationsIcon from "../templates/elements/icon/assets/notifications.inline.svg";
import PaycheckIcon from "../templates/elements/icon/assets/paycheck.inline.svg";
import SignOutIcon from "../templates/elements/icon/assets/signOut.inline.svg";
import TilesIcon from "../templates/elements/icon/assets/tiles.inline.svg";
import WalletIcon from "../templates/elements/icon/assets/wallet.inline.svg";

export {
    GearIcon,
    HomeIcon,
    LifebuoyIcon,
    PaycheckIcon,
    SignOutIcon,
    TilesIcon,
    WalletIcon,
    AngleLeftIcon,
    NotificationsIcon,
    AngleBottomIcon,
    FileCSVIcon,
    FileXMLIcon,
    FileJSONIcon,
    FilesIcon,
    HamburgerIcon,
};
