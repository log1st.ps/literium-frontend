import classNames from "classnames";
import {ReactComponentLike} from "prop-types";
import React, { Fragment } from "react";
import {UrlObject} from "url";
import {ActiveLink} from "../../helpers/ActiveLink";
import Logo from "./assets/logo.inline.svg";
import styles from "./side.scss";

interface ISideMenuItem {
    key: string;
    Icon: ReactComponentLike;
    label: string;
    onClick?(): void;
    route?: string | UrlObject;
}

export interface ISide {
    onLogoClick?(): void;
    menuItems?: Array<ISideMenuItem | "delimiter">;
}

const MenuItemInner = ({
    Icon,
    ...item
}: ISideMenuItem) => (
    <Fragment>
        <Icon className={styles.icon}/>
        <div className={styles.label}>
            {item.label}
        </div>
    </Fragment>
);

const MenuItem = ({
    route,
    onClick,
    ...item
}: ISideMenuItem) => (
    route ? (
        <ActiveLink activeClassName={styles.activeItem} href={route}>
            <a
                onClick={onClick}
                className={classNames({
                    [styles.item]: true,
                })}
            >
                <MenuItemInner {...item}/>
            </a>
        </ActiveLink>
    ) : (
        <button onClick={onClick} className={styles.item}>
            <MenuItemInner {...item}/>
        </button>
    )
);

const Side = ({
    onLogoClick,
    menuItems,
}: ISide) => (
    <div className={styles.container}>
        <button onClick={onLogoClick} className={styles.logo}>
            <Logo className={styles.logoImage}/>
        </button>
        <div className={styles.menu}>
            {menuItems.map((item, index) => (
                item === "delimiter"
                    ? (<div key={index} className={styles.delimiter}/>)
                    : (<MenuItem key={item.key} {...item}/>)
            ))}
        </div>
    </div>
);

export {
    Side,
};
