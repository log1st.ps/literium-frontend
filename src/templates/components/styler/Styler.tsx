import {ReactNode} from "react";
import {renderSlot} from "../../../helpers/renderSlot";

interface IStyler {
    cursor?: string;

    height?: number | string;
    width?: number | string;

    children?: ReactNode;
}

const Styler = ({
    cursor,
    width,
    height,
    children,
}: IStyler) => (
    <div
        style={{
            cursor,
            height,
            width,
        }}
    >
        {renderSlot(children)}
    </div>
);

export {
    Styler,
};
