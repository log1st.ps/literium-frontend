import classNames from "classnames";
import React from "react";
import styles from "./divider.scss";

interface IDivider {
    margin?: number | string;
    otherMargin?: number | string;
    isDark?: boolean;
    height?: string | number;
    width?: string | number;
}

const Divider = ({
    margin,
    otherMargin,
    isDark,
    height= 1,
    width= "100%",
}: IDivider) => (
    <div
        className={classNames({
            [styles.divider]: true,
            [styles.isDark]: isDark,
        })}
        style={{
            marginTop: margin,
            marginBottom: margin,
            marginLeft: otherMargin,
            marginRight: otherMargin,
            height,
            width,
        }}
    />
);

export {
    Divider,
};
