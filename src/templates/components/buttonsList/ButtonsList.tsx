import classNames from "classnames";
import React from "react";
import {calculateGridGap} from "../../../helpers/calculateGridGap";
import {Button, IButton} from "../../elements/button/Button";
import styles from "./buttonsList.scss";

interface IConnectedButton extends IButton {
    key: string;
    label?: string;
}

export interface IButtonsList {
    buttons: IConnectedButton[];
    gap?: number;
    direction?: "column" | "row" | "rowReverse" | "columnReverse";
    align?: "start" | "center" | "end";
    justify?: "start" | "center" | "end";
}

const ButtonsList = ({
    buttons,
    gap= 0,
    direction = "row",
    align,
    justify,
}: IButtonsList) => (
    <div
        className={classNames({
            [styles.list]: true,
            [styles[`${direction}Direction`]]: true,
            [styles[`${align}Align`]]: !!align,
            [styles[`${justify}Justify`]]: !!justify,
        })}
        style={calculateGridGap({gap: -gap, direction})}
    >
        {buttons.map(({key, label, ...button}) => (
            <div
                key={key}
                className={classNames({
                    [styles.button]: true,
                    [styles.buttonFullWidth]: button.isBlock,
                })}
                style={calculateGridGap({gap, direction})}
            >
                <Button {...button}>
                    {label}
                </Button>
            </div>
        ))}
    </div>
);

export {
    ButtonsList,
};
