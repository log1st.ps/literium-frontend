import classNames from "classnames";
import React from "react";
import {compose} from "recompose";
import {withTranslation} from "../../../hoc/withTranslation";
import {ITranslation} from "../../../interfaces/ITranslation";
import {IUser} from "../../../interfaces/IUser";
import {Icon} from "../../elements/icon/Icon";
import {H3} from "../typography/Typography";
import styles from "./header.scss";

export interface IHeader {
    previousPage?: {
        href: string;
        label: string;
    };

    notificationsCount: number;
    onNotificationsClick?(): void;

    onBackButtonClick?(): void;
    texts: ITranslation["header"];

    user?: IUser;

    isMobile?: boolean;

    pageTitle?: string;
}

const Header = compose<any, any>(
    withTranslation("header"),
)(
({
    previousPage,
    onBackButtonClick,
    onNotificationsClick,
    texts,
    notificationsCount,
    user,
    isMobile,
    pageTitle,
}: IHeader) => (
        <div
            className={classNames({
                [styles.header]: true,
                [styles.isMobile]: isMobile,
            })}
        >
            {isMobile && (
                <button className={styles.menuToggle}>
                    <Icon type={"hamburger"} size={"sm"}/>
                </button>
            )}
            {pageTitle && (
                <div className={styles.title}>
                    <H3 withEqualLine={isMobile} isBold={true}>{pageTitle}</H3>
                    <div className={styles.titleBack}>{pageTitle}</div>
                </div>
            )}
            {/*{previousPage && (*/}
            {/*    <Link href={previousPage.href}>*/}
            {/*        <a onClick={onBackButtonClick} className={styles.backButton}>*/}
            {/*            <div className={styles.backIcon}>*/}
            {/*                <Icon type={"angleLeft"} size={"sm"}/>*/}
            {/*            </div>*/}
            {/*            <span className={styles.backLabel}>*/}
            {/*                {texts.back.messageFormat({page: previousPage.label})}*/}
            {/*            </span>*/}
            {/*        </a>*/}
            {/*    </Link>*/}
            {/*)}*/}
            <div className={styles.side}>
                <button
                    onClick={onNotificationsClick}
                    className={styles.notifications}
                >
                    <Icon type={"notifications"} size={"xsm"}/>
                    {!!notificationsCount && (
                        <span className={styles.notificationsCount}>{notificationsCount}</span>
                    )}
                </button>
                {user && (
                    <button className={styles.user}>
                        <span className={styles.userAvatar} style={{backgroundImage: `url(${user.avatarUrl})`}}/>
                        <div className={styles.userIcon}>
                            <Icon type={"angleBottom"} size={"xs"}/>
                        </div>
                    </button>
                )}
            </div>
        </div>
    ),
);

export {
    Header,
};
