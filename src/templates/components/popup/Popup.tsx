import classNames from "classnames";
import React, {ReactNode, RefObject} from "react";
import {compose} from "recompose";
import {renderSlot} from "../../../helpers/renderSlot";
import {withForwardedRef} from "../../../hoc/withForwardedRef";
import {Card} from "../card/Card";
import {H5} from "../typography/Typography";
import styles from "./popup.scss";

interface IPopup {
    title?: string;
    children?: ReactNode;
    arrowType?: "top" | "left" | "right" | "bottom";
    width?: number | string;
    overflowHidden?: boolean;
    overflowScroll?: boolean;
    forwardedRef?: {
        arrow: RefObject<any>,
        content: RefObject<any>,
    };
    withMoreShadow?: boolean;
    arrowLeft?: number | string;
    arrowTop?: number | string;
}

const Inner = ({
    children,
    title,
    arrowType,
    width,
    overflowHidden,
    overflowScroll,
    forwardedRef,
    withMoreShadow,
    arrowLeft,
    arrowTop,
}: IPopup) => (
    <div
        className={styles.popup}
        style={{
            width,
        }}
        ref={forwardedRef && forwardedRef.content}
    >
        {arrowType && (
            <div
                className={classNames({
                    [styles.arrow]: true,
                    [styles[`${arrowType}Arrow`]]: true,
                })}
                style={{
                    left: arrowLeft,
                    top: arrowTop,
                }}
                ref={forwardedRef && forwardedRef.arrow}
            />
        )}
        <Card
            isMobile={true}
            overflowScroll={overflowScroll}
            overflowHidden={overflowHidden}
            withMoreShadow={withMoreShadow}
        >
            {title && (
                <div className={styles.title}>
                    <H5 isBold={true} color={"dark"}>
                        {title}
                    </H5>
                    <div className={styles.titleBack}>
                        {title}
                    </div>
                </div>
            )}
            {children && (
                <div className={styles.content}>
                    {renderSlot(children)}
                </div>
            )}
        </Card>
    </div>
);

const Popup = compose<any, IPopup>(
    withForwardedRef(),
)(Inner);

export {
    Popup,
};
