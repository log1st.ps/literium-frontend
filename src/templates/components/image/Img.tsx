import classNames from "classnames";
import {ReactComponentLike} from "prop-types";
import React from "react";
import {compose, withProps} from "recompose";
import styles from "./image.scss";

interface IImage {
    url: string;
    alt?: string;
    height?: number | string;
    width?: number | string;
    type?: "block" | "img";
    backgroundType?: "cover" | "contain" | "initial" | number | string;
    borderRadius?: number | string;
}

interface IInnerImage extends IImage {
    Component: ReactComponentLike;
}

const Inner = ({
    Component,
    url,
    alt,
    height,
    width,
    backgroundType = "initial",
    type,
    borderRadius,
}: IInnerImage) => (
    <Component
        className={classNames({
            [styles.image]: true,
            [styles[`${type}Type`]]: true,
            [styles[`${backgroundType}BackgroundType`]]: type === "block",
        })}
        src={type === "img" ? url : undefined}
        alt={type === "img" ? alt : undefined}
        style={{
            width,
            height,
            borderRadius,
            backgroundImage: type === "block" && `url(${url})`,
        }}
    />
);

const Img = compose<any, IImage>(
    withProps(({type = "img"}) => ({
        type,
        Component: {
            block: "div",
            img: "img",
        }[type],
    })),
)(Inner);

export {
    Img,
};
