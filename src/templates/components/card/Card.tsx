import classNames from "classnames";
import React, {ReactNode} from "react";
import {renderSlot} from "../../../helpers/renderSlot";
import styles from "./card.scss";

interface ICard {
    children?: ReactNode;
    isMobile?: boolean;
    overflowHidden?: boolean;
    overflowScroll?: boolean;
    withMoreShadow?: boolean;
}

const Card = ({
    children,
    isMobile,
    overflowHidden,
    overflowScroll,
    withMoreShadow,
}: ICard) => (
    <div
        className={classNames({
            [styles.container]: true,
            [styles.isMobile]: isMobile,
            [styles.overflowHidden]: overflowHidden,
            [styles.overflowScroll]: overflowScroll,
            [styles.withMoreShadow]: withMoreShadow,
        })}
    >
        {renderSlot(children)}
    </div>
);

export {
    Card,
};
