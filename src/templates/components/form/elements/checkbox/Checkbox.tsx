import classNames from "classnames";
import React from "react";
import {compose, withHandlers, withProps} from "recompose";
import {Icon} from "../../../../elements/icon/Icon";
import {P} from "../../../typography/Typography";
import {IFormField} from "../IFormField";
import styles from "./checkbox.scss";

interface ICheckbox extends IFormField {
    trueValue?: any;
    falseValue?: any;
}

const Inner = ({
    value,
    label,
    trueValue,
    isDisabled,
    isReadonly,
    onInput,
    isMobile,
}: ICheckbox) => (
    <label
        className={classNames({
            [styles.container]: true,
            [styles.isDisabled]: isDisabled,
            [styles.isReadonly]: isReadonly,
            [styles.isMobile]: isMobile,
        })}
        onClick={onInput}
    >
        <div
            className={classNames({
                [styles.checkbox]: true,
                [styles.isChecked]: value === trueValue,
            })}
        >
            {value === trueValue && (<Icon size={"sm"} type={"check"}/>)}
        </div>
        <div className={styles.label}>
            <P>
                {label}
            </P>
        </div>
    </label>
);

const Checkbox = compose<any, ICheckbox>(
    withProps(({trueValue, falseValue}) => ({
        trueValue: typeof trueValue === "undefined" ? true : trueValue,
        falseValue: typeof falseValue === "undefined" ? false : falseValue,
    })),
    withHandlers({
        onInput: ({onInput, value, trueValue, falseValue, isReadonly, isDisabled}) => () => {
            if (onInput && !isReadonly && !isDisabled) {
                onInput(value === trueValue ? falseValue : trueValue);
            }
        },
    }),
)(Inner);

export {
    Checkbox,
};
