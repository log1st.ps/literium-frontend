import React, {ReactNode, RefObject} from "react";
import {IField} from "./field/Field";

export interface IFormField extends IField {
    value?: any;
    onInput?(value: any): void;
    placeholder?: string;
    isDisabled?: boolean;
    isReadonly?: boolean;
    isMobile?: boolean;
    before?: ReactNode;
    after?: ReactNode;
    hint?: string;

    hasAppearance?: boolean;
    isLight?: boolean;

    height?: string | number;
    width?: string | number;

    onFocus?(): void;
    onBlur?(): void;

    isOutsideFocused?: boolean;
}

export interface IInnerFormField extends IFormField {
    isFocused: boolean;
    focus(): void;
    blur(): void;
    inputRef: RefObject<HTMLElement>;
    contentRef: RefObject<HTMLElement> | any;
    forwardedRef?: React.RefObject<any>;
}
