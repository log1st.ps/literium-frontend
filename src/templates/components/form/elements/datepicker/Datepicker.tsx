import classNames from "classnames";
import React from "react";
import {compose, withHandlers, withProps} from "recompose";
import {datepickerFormat} from "../../../../../config";
import {Field} from "../field/Field";
import {IFormField, IInnerFormField} from "../IFormField";
import styles from "./datepicker.scss";

interface IDatepicker extends IFormField {
    format: string;
}

interface IInnerDatepicker extends IInnerFormField, IDatepicker {

}

const Inner = ({
    value,
    label,
    isDisabled,
    isReadonly,
    onInput,
    placeholder,
    isMobile,
    isFocused,
    hasAppearance,
    isLight,
    error,
    width,
    height,
    focus,
    format,
}: IInnerDatepicker) => (
    <Field label={label} error={error}>
        <div
            className={classNames({
                [styles.container]: true,
                [styles.isDisabled]: isDisabled,
                [styles.isReadonly]: isReadonly,
                [styles.isMobile]: isMobile,
                [styles.isFocused]: isFocused,
                [styles.noAppearance]: !hasAppearance,
                [styles.isLight]: isLight,
            })}
            style={{
                height,
                width,
            }}
            onClick={focus}
        />
    </Field>
);

const Datepicker = compose<any, IDatepicker>(
    withProps(({format}) => ({
        format: format || datepickerFormat,
    })),
    withHandlers({
        onInput: ({onInput, isReadonly, isDisabled}) => (value) => {
            if (onInput && !isReadonly && !isDisabled) {
                onInput(value);
            }
        },
    }),
)(Inner);

export {
    Datepicker,
};
