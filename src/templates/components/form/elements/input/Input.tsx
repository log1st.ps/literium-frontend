import classNames from "classnames";
import {ReactComponentLike} from "prop-types";
import React from "react";
import {compose, withHandlers, withProps} from "recompose";
import {renderSlot} from "../../../../../helpers/renderSlot";
import {P} from "../../../typography/Typography";
import {asCommonFormElement} from "../asCommonFormElement";
import {Field} from "../field/Field";
import {IFormField, IInnerFormField} from "../IFormField";
import styles from "./input.scss";

interface IInput extends IFormField {
    type?: "text" | "number" | "textarea" | "password";
}

interface IInnerInput extends IInnerFormField, IInput {
    Component?: ReactComponentLike;
}

const Inner = ({
    type,
    placeholder,
    value,
    onInput,
    isDisabled,
    isReadonly,
    Component,
    isMobile,
    height,
    width,
    isFocused,
    focus,
    blur,
    inputRef,
    label,
    error,
    hint,
    hasAppearance = true,
    isLight = false,
    before,
    after,
    contentRef,
}: IInnerInput) => (
    <Field label={label} error={error}>
        <div
            className={classNames({
                [styles.container]: true,
                [styles.isDisabled]: isDisabled,
                [styles.isReadonly]: isReadonly,
                [styles.isMobile]: isMobile,
                [styles.isFocused]: isFocused,
                [styles.noAppearance]: !hasAppearance,
                [styles.isLight]: isLight,
            })}
            style={{
                height,
                width,
            }}
            onClick={focus}
        >
            <div
                className={styles.content}
                tabIndex={0}
                ref={contentRef}
            >
                {before && (
                    <div className={styles.before} tabIndex={0}>
                        {renderSlot(before)}
                    </div>
                )}
                <Component
                    type={type}
                    placeholder={placeholder}
                    value={value}
                    onChange={onInput}
                    className={styles.input}
                    readOnly={isReadonly}
                    disabled={isDisabled}
                    onFocus={focus}
                    onBlur={blur}
                    ref={inputRef}
                />
                {after && (
                    <div className={styles.after}>
                        {renderSlot(after)}
                    </div>
                )}
            </div>
            {hint && (
                <div className={styles.hint} tabIndex={0}>
                    <P color={"grey"}>{hint}</P>
                </div>
            )}
        </div>
    </Field>
);

const Input = compose<any, IInput>(
    asCommonFormElement,
    withProps(({type}) => ({
        Component: type === "textarea" ? "textarea" : "input",
    })),
    withHandlers({
        onInput: ({onInput}) => ({target: {value}}) => {
            if (onInput) {
                onInput(value);
            }
        },
    }),
)(Inner);

export {
    Input,
};
