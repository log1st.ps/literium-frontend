import {ReactNode} from "react";
import {renderSlot} from "../../../../../helpers/renderSlot";
import {Icon} from "../../../../elements/icon/Icon";
import {H6, P, S} from "../../../typography/Typography";
import styles from "./field.scss";

export interface IField {
    label?: string;
    error?: string | string[];
}

interface IInnerField extends IField {
    children?: ReactNode;
}

const Field = ({
    children,
    error,
    label,
}: IInnerField) => (
    <div className={styles.container}>
        {label && (
            <div className={styles.label}>
                <H6 color={"grey"}>{label}</H6>
            </div>
        )}
        {children && (
            <div className={styles.field}>
                {renderSlot(children)}
            </div>
        )}
        {error && (
            <div className={styles.error}>
                <Icon type={"exclamationPoint"} size={"sm"}/>
                <div className={styles.errorList}>
                    <P isMedium={true} color={"tosca"}>
                        {(Array.isArray(error) ? error : [error]).map((err, index) => (
                            <div key={index}>{err}</div>
                        ))}
                    </P>
                </div>
            </div>
        )}
    </div>
);

export {
    Field,
};
