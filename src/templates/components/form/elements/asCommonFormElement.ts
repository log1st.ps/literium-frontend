import {createRef} from "react";
import {compose, lifecycle, withHandlers, withProps, withState} from "recompose";
import {IInnerFormField} from "./IFormField";

const asCommonFormElement = compose(
    withProps(() => ({
        inputRef: createRef(),
        contentRef: createRef(),
    })),
    withState("isFocused", "setIsFocused", false),
    withHandlers({
        focus: ({setIsFocused, inputRef, onFocus}) => () => {
            setIsFocused(true);

            if (inputRef && inputRef.current) {
                inputRef.current.focus();
            }

            if (onFocus) {
                onFocus();
            }
        },
    }),
    withHandlers({
        blur: ({setIsFocused, onBlur, focus, contentRef: {current}}) => ({relatedTarget}) => {
            if (current === relatedTarget || current.contains(relatedTarget)) {
                focus();
                return false;
            }
            setIsFocused(false);

            if (onBlur) {
                onBlur();
            }
        },
    }),
    lifecycle({
        componentDidMount() {
            const {
                isOutsideFocused,
                focus,
            } = this.props as IInnerFormField;

            if (isOutsideFocused) {
                focus();
            }
        },
        shouldComponentUpdate({isOutsideFocused, focus, blur}) {
            if ((this.props as any).isOutsideFocused !== isOutsideFocused) {
                if (isOutsideFocused) {
                    focus();
                } else {
                    blur();
                }
            }
            return true;
        },
    }),
);

export {
    asCommonFormElement,
};
