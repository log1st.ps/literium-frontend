import React, {ReactNode} from "react";
import {renderSlot} from "../../../helpers/renderSlot";
import {Checkbox} from "./elements/checkbox/Checkbox";
import {Input} from "./elements/input/Input";
import styles from "./form.scss";

interface IForm {
    children?: ReactNode;
}

const Form = ({
    children,
}: IForm) => (
    <form className={styles.form}>
        {renderSlot(children)}
    </form>
);

Form.Checkbox = Checkbox;
Form.Input =  Input;

export {
    Form,
};
