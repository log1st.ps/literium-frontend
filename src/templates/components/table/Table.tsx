import classNames from "classnames";
import React, {createRef, Fragment, ReactNode} from "react";
import {createPortal} from "react-dom";
import {compose, lifecycle, withHandlers, withProps, withState} from "recompose";
import {observePopup} from "../../../helpers/observePopup";
import {Button} from "../../elements/button/Button";
import {Icon, IIcon} from "../../elements/icon/Icon";
import {Margin} from "../../layouts/grid/Grid";
import {ButtonsList, IButtonsList} from "../buttonsList/ButtonsList";
import {Display} from "../display/Display";
import {Divider} from "../divider/Divider";
import {Popup} from "../popup/Popup";
import {ICommonTypography, P, S} from "../typography/Typography";
import styles from "./table.scss";

export interface ITableColumn {
    key: string;
    label?: string;
    width?: string;
}

export interface ITableData {
    raw: {
        [key: string]: {
            value?: string;
            hint?: string;
        };
    };
    index: number;
}

export interface ITableAction {
    key: string;
    label: string;
    handler?(data: ITableData): void;
    color?: ICommonTypography["color"];
    icon?: IIcon["type"];
}

export interface ITableSlots {
    renderHeader?(columns: ITableColumn[]): ReactNode;
    renderHeaderCell?: {
        [key: string]: (column: ITableColumn) => ReactNode,
    };
    renderRow?(
        columns: ITableColumn[],
        row: ITableData,
        onClick?: (row: ITableData, index: number) => void,
        onDbClick?: (row: ITableData, index: number) => void,
    ): ReactNode;
    renderRowCell?: {
        [key: string]: (column: ITableColumn, row: ITableData) => ReactNode,
    };
    renderRowCellInner?: {
        [key: string]: (column: ITableColumn, row: ITableData) => ReactNode,
    };
    onRowClick?(row: ITableData, index: number): () => void;
}

export interface ITable extends ITableSlots {
    columns: Array<string | ITableColumn>;
    data: ITableData[];
    keyField: IInnerTable["keyField"];
    isMobile?: boolean;
    actions?: Array<{}>;
    footerActions?: IButtonsList["buttons"];
}

interface IInnerTable extends ITable {
    columns: ITableColumn[];
    data: ITableData[];
    keyField: string;
    isMobile?: boolean;
}

const TableActions = compose<any, {
    actions: ITable["actions"],
    isMobile: boolean;
    row: ITableData;
}>(
    withState("isActive", "setIsActive", false),
    withState("observer", "setObserver", null),
    withProps(() => ({
        targetRef: createRef(),
        popupRef: createRef(),
        arrowRef: createRef(),
    })),
    withHandlers({
        show: ({setIsActive}) => () => {
            setIsActive(true);
        },
        hide: ({setIsActive}) => () => {
            setIsActive(false);
        },
        toggle: ({setIsActive, isActive}) => (e) => {
            e.stopPropagation();
            setIsActive(!isActive);
        },
        handleObserver: ({isMobile, observer}) => () => {
            if (observer) {
                observer.align = isMobile ? "end" : "center";
                observer.otherOffset = isMobile ? 30 : 0;
            }
        },
    }),
    lifecycle({
        shouldComponentUpdate({isMobile, handleObserver}) {
            handleObserver(isMobile);

            return true;
        },
        componentDidMount() {
            setTimeout(() => {
                const {
                    targetRef,
                    popupRef,
                    arrowRef,
                    hide,
                    setObserver,
                } = this.props as any;

                setObserver(
                    observePopup({
                        target: targetRef.current,
                        popup: popupRef.current,
                        arrow: arrowRef.current,
                        isFixed: true,
                        align: "center",
                        offset: 4,
                        safeOutsideElements: [
                            targetRef.current,
                        ],
                        onOutsideClick(): void {
                            hide();
                        },
                    }),
                );

                setTimeout(() => {
                    const {handleObserver} = this.props as any;
                    handleObserver();
                });
            });
        },
    }),
)(
    ({
        targetRef,
        popupRef,
        arrowRef,
        isActive,
        toggle,
        actions,
        row,
    }: any) => (
        <div className={styles.actions}>
            <Button
                ref={targetRef}
                renderBefore={(
                    <Icon type={"dots"} size={"sm"}/>
                )}
                state={"pure"}
                onClick={toggle}
            />
            {createPortal(
                (
                    <Display
                        visibility={isActive ? "visible" : "hidden"}
                        opacity={isActive ? 1 : 0}
                        ref={popupRef}
                    >
                        <Popup
                            overflowScroll={true}
                            width={120}
                            arrowType={"top"}
                            ref={{
                                arrow: arrowRef,
                            } as any}
                            withMoreShadow={true}
                        >
                            <ButtonsList
                                buttons={actions.map(({
                                    key, label, handler, color, icon,
                                }) => ({
                                    key,
                                    label: (
                                        <Margin left={-4}>
                                            <P weight={600} color={color}>{label}</P>
                                        </Margin>
                                    ),
                                    onClick: (e) => {
                                        e.stopPropagation();

                                        handler(row)();
                                    },
                                    state: "pure",
                                    renderBefore: (
                                        <Icon type={icon} size={"sm"}/>
                                    ),
                                }))}
                            />
                        </Popup>
                    </Display>
                ),
                document.body,
            )}
        </div>
    ),
);

const InnerTable = ({
    columns,
    data,
    keyField,
    isMobile,

    renderHeader,
    renderHeaderCell,
    renderRow,
    renderRowCell,
    renderRowCellInner,

    actions,
    footerActions,
    onRowClick,
}: IInnerTable) => (
    <div
        className={classNames({
            [styles.table]: true,
            [styles.isMobile]: isMobile,
        })}
    >
        <table>
            <thead>
            {renderHeader ? renderHeader(columns) : (
                <tr>
                    {columns.map(({key, label, width}) => (
                        (renderHeaderCell && key in renderHeaderCell) ? (
                            <Fragment key={key}>
                                {renderHeaderCell[key]({key, label, width})}
                            </Fragment>
                        ) : (
                            <th key={key} style={width && {width}}>
                                <P color={"dark"} isBold={true}>
                                    {label}
                                </P>
                            </th>
                        )
                    ))}
                    {actions && (
                        <th className={styles.actionsCell}/>
                    )}
                </tr>
            )}
            </thead>
            <tbody>
            {data.map((row, index) => (
                renderRow ? renderRow(columns, row, onRowClick(row, index)) : (
                    <Fragment key={index}>
                        <tr
                            onClick={onRowClick(row, index)}
                            className={classNames({
                                [styles.rowClickable]: !!onRowClick(row, index),
                            })}
                        >
                            {columns.map(({key, ...column}, colIndex) => (
                                (renderRowCell && key in renderRowCell) ? (
                                    <Fragment key={colIndex}>
                                        {renderRowCell[key]({key, ...column}, row)}
                                    </Fragment>
                                ) : (
                                    <td key={colIndex}>
                                        {renderRowCellInner && key in renderRowCellInner ? (
                                            renderRowCellInner[key]({key, ...column}, row)
                                        ) : (
                                            <Fragment>
                                                {key in row.raw && (
                                                    <Fragment>
                                                        {"value" in row.raw[key] && (
                                                            <div className={styles.value}>
                                                                <P color={"dark"}>
                                                                    {row.raw[key].value}
                                                                </P>
                                                            </div>
                                                        )}
                                                        {"hint" in row.raw[key] && (
                                                            <div className={styles.hint}>
                                                                {"value" in row.raw[key] ? (
                                                                    <S color={"grey"}>
                                                                        {row.raw[key].hint}
                                                                    </S>
                                                                ) : (
                                                                    <P color={"grey"}>
                                                                        {row.raw[key].hint}
                                                                    </P>
                                                                )}
                                                            </div>
                                                        )}
                                                    </Fragment>
                                                )}
                                            </Fragment>
                                        )}
                                    </td>
                                )
                            ))}
                            {actions && (
                                <td className={styles.actionsCell}>
                                    <TableActions row={row} isMobile={isMobile} actions={actions}/>
                                </td>
                            )}
                        </tr>
                        <tr className={styles.divider}>
                            <td colSpan={columns.length + (actions ? 1 : 0)}>
                                <Divider isDark={true}/>
                            </td>
                        </tr>
                    </Fragment>
                )
            ))}
            </tbody>
            {footerActions && (
                <tfoot>
                <tr>
                    <td colSpan={columns.length + (actions ? 1 : 0)}>
                        <ButtonsList
                            buttons={footerActions}
                            justify={"center"}
                        />
                    </td>
                </tr>
                </tfoot>
            )}
        </table>
    </div>
);

const Table = compose<any, any>(
    withProps<any, ITable>(
        ({columns, data}) => {

            return {
                columns: (columns || [])
                    .map((column) => (typeof column === "string" ? {key: column, label: column} : column)),
                data: (data || [])
                    .map(({raw, index}) => ({
                        index,
                        raw: Object.entries(raw).reduce((previous, [key, value]) => ({
                            ...previous,
                            [key]: typeof value === "object" ? value : {value},
                        }), {}),
                    })),
            };
        },
    ),
    withState("dbClicking", "setDbClicking", {}),
    withHandlers( {
        onRowClick: ({onRowClick, setDbClicking, dbClicking}) => (row, index) => () => {
            if (!(index in dbClicking)) {
                const selectionBefore = window.getSelection();
                // console.log(selectionBefore);
                if (selectionBefore.type === "Range") {
                    return;
                }
                const timeout = setTimeout(() => {
                    const selection = window.getSelection();
                    if (selection.type !== "Range") {
                        // console.log("go");
                        onRowClick(row)();
                    }
                }, 500);

                setDbClicking({
                    ...dbClicking,
                    [index]: timeout,
                });
            }   else {
                clearTimeout(dbClicking[index]);

                const {[index]: heh, ...otherClicks} = dbClicking;

                setDbClicking(otherClicks);
            }
        },
    }),
)(InnerTable);

export {
    Table,
};
