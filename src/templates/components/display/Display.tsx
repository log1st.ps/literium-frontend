import classNames from "classnames";
import {ReactNode, RefObject} from "react";
import {compose} from "recompose";
import {renderSlot} from "../../../helpers/renderSlot";
import {withForwardedRef} from "../../../hoc/withForwardedRef";
import styles from "./display.scss";

interface IDisplay {
    visibility: "hidden" | "visible";
    opacity: number;
    children?: ReactNode;
    forwardedRef?: RefObject<any>;
}

const Inner = ({
    visibility = "visible",
    opacity = 1,
    children,
    forwardedRef,
}: IDisplay) => (
    <div
        className={classNames({
            [styles.display]: true,
        })}
        style={{
            visibility,
            opacity,
        }}
        ref={forwardedRef}
    >
        {renderSlot(children)}
    </div>
);

const Display = compose<any, IDisplay>(
    withForwardedRef(),
)(Inner);

export {
    Display,
};
