import {ReactNode} from "react";
import {renderSlot} from "../../../helpers/renderSlot";
import styles from "./alert.scss";

interface IAlert {
    children?: ReactNode;
}

const Alert = ({
    children,
}: IAlert) => (
    <div className={styles.alert}>
        <div className={styles.icon}>!</div>
        <div className={styles.content}>
            {renderSlot(children)}
        </div>
    </div>
);

export {
    Alert,
};
