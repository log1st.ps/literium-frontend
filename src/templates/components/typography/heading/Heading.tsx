import classNames from "classnames";
import React, {ReactNode} from "react";
import styles from "./heading.scss";

export interface IHeading {
    children: ReactNode;
    isMobile?: boolean;
}

const Heading = ({
    children,
    isMobile,
}: IHeading) => (
    <div
        className={classNames({
            [styles.heading]: true,
            [styles.isMobile]: isMobile,
        })}
    >
        {children}
    </div>
);

export {
    Heading,
};
