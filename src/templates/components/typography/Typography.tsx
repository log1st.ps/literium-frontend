import classNames from "classnames";
import {withProps} from "recompose";
import {renderSlot} from "../../../helpers/renderSlot";
import styles from "./typography.scss";

import colors from "../../../assets/variables.scss";
const colorNames = Object.keys(colors);

export interface ICommonTypography {
    text?: string;
    children?: string | string[] | any;
    isBold?: boolean;
    isMedium?: boolean;
    isRegular?: boolean;
    align?: "left" | "center" | "right";
    color?: typeof colorNames[number];
    withEqualLine?: boolean;
    weight?: number;
}

const availableTypographyTypes = [
    "h1", "h2", "h3", "h4", "h5", "h6", "p", "s", "ss",
] as const;

interface IBaseTypography extends ICommonTypography {
    type?: typeof availableTypographyTypes[number];
}

const Typography = ({
    text,
    type,
    children,
    isBold,
    isMedium,
    isRegular,
    align = "left",
    color,
    withEqualLine,
    weight,
}: IBaseTypography) => (
    <div
        className={classNames({
            [styles.typography]: true,
            [styles[type]]: !!type,
            [styles.isBold]: isBold,
            [styles.isMedium]: isMedium,
            [styles.isRegular]: isRegular,
            [styles.withEqualLine]: withEqualLine,
            [styles[`${align}Align`]]: !!align,
        })}
        style={{
            color: colors[color],
            fontWeight: weight,
        }}
    >
        {renderSlot(children, text)}
    </div>
);

const H1 = withProps<any, ICommonTypography>({type: "h1"})(Typography as any);
const H2 = withProps<any, ICommonTypography>({type: "h2"})(Typography as any);
const H3 = withProps<any, ICommonTypography>({type: "h3"})(Typography as any);
const H4 = withProps<any, ICommonTypography>({type: "h4"})(Typography as any);
const H5 = withProps<any, ICommonTypography>({type: "h5"})(Typography as any);
const H6 = withProps<any, ICommonTypography>({type: "h6"})(Typography as any);
const P = withProps<any, ICommonTypography>({type: "p"})(Typography as any);
const S = withProps<any, ICommonTypography>({type: "s"})(Typography as any);
const SS = withProps<any, ICommonTypography>({type: "ss"})(Typography as any);

export {
    H1,
    H2,
    H3,
    H4,
    H5,
    H6,
    P,
    S,
    SS,
};
