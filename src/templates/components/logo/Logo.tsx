import LogoIcon from "./assets/logo.inline.svg";
import styles from "./logo.scss";

interface ILogo {
    height?: number;
    width?: number;
}

const Logo = ({
    width= 88,
    height = 21,
}: ILogo) => (
    <div
        className={styles.logo}
        style={{
            height,
            width,
        }}
    >
        <LogoIcon className={styles.image}/>
    </div>
);

export {
    Logo,
};
