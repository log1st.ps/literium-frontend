import classNames from "classnames";
import {ReactNode} from "react";
import styles from "./widget.scss";

interface IWidget {
    title?: string;
    children?: ReactNode;
    type?: "body" | "table";
    isMobile?: boolean;
}

const Widget = ({
    title,
    children,
    type= "body",
    isMobile,
}: IWidget) => (
    <div
        className={classNames({
            [styles.widget]: true,
            [styles[`${type}Type`]]: true,
            [styles.isMobile]: isMobile,
        })}
    >
        {title && (
            <div className={styles.header}>
                {title}
            </div>
        )}
        <div className={styles.body}>
            {children}
        </div>
    </div>
);

export {
    Widget,
};
