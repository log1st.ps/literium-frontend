import classNames from "classnames";
import React, {createRef, ReactNode, RefObject} from "react";
import {createPortal} from "react-dom";
import {compose, lifecycle, withProps, withState} from "recompose";
import {renderSlot} from "../../../helpers/renderSlot";
import {withMobileObserver} from "../../../hoc/withMobileObserver";
import styles from "./modal.scss";

interface IModal {
    children?: ReactNode;
    isActive?: boolean;
    onClose?(): void;

    portalTarget?: string;

    width?: number;
    isMobile?: boolean;
}

interface IInnerModal extends IModal {
    outsideRef: RefObject<HTMLElement> | any;
    dialogRef: RefObject<HTMLElement> | any;

    setClickHandler?(clickHandler: (e: any) => void): void;
    clickHandler?(): void;
}

let activeModalsLength = 0;

const handleBodyScroll = () => {
    document.documentElement.style.overflowY = activeModalsLength ? "hidden" : "initial";
};

const Inner = ({
    children,
    isActive,
    outsideRef,
    dialogRef,
    portalTarget,
    width = 500,
    isMobile,
}: IInnerModal) => (
    createPortal((
        <div
            className={classNames({
                [styles.modal]: true,
                [styles.isActive]: isActive,
                [styles.isMobile]: isMobile,
            })}
            ref={outsideRef}
        >
            <div
                className={styles.container}
                style={{maxWidth: width}}
            >
                <div className={styles.dialog} ref={dialogRef}>
                    {renderSlot(children)}
                </div>
            </div>
        </div>
    ), portalTarget)
);

const attachClickHandler = ({dialogRef, onClose}) => (e) => {
    if (!e) {
        return;
    }

    const {target, currentTarget} = e;

    if (dialogRef === target) {
        return;
    }

    if (dialogRef.contains(target)) {
        return;
    }

    if (onClose) {
        onClose();
    }
};

const Modal = compose<any, IModal>(
    withProps(() => ({
        outsideRef: createRef(),
        dialogRef: createRef(),
    })),
    withProps(({portalTarget}) => ({
        portalTarget: !portalTarget
            ? document.body
            : (typeof portalTarget !== "object" ? document.querySelector(portalTarget) : portalTarget),
    })),
    withState("clickHandler", "setClickHandler", null),
    withMobileObserver,
    lifecycle({
        componentDidMount() {
            const {
                setClickHandler,
                dialogRef,
                onClose,
                outsideRef,
                isActive,
            } = this.props as IInnerModal;

            const clickHandler = attachClickHandler({dialogRef: dialogRef.current, onClose});

            setClickHandler(clickHandler);
            outsideRef.current.addEventListener("click", clickHandler);

            if (isActive) {
                activeModalsLength += 1;
                handleBodyScroll();
            }
        },
        componentWillUnmount() {
            const {
                clickHandler,
                outsideRef,
                isActive,
            } = this.props as IInnerModal;

            outsideRef.current.removeEventListener("click", clickHandler);

            if (isActive) {
                activeModalsLength -= 1;
                handleBodyScroll();
            }
        },
        shouldComponentUpdate({isActive}) {
            if (isActive !== (this.props as IModal).isActive) {
                activeModalsLength += isActive ? 1 : -1;
                handleBodyScroll();
            }

            return true;
        },
    }),
)(Inner);

export {
    Modal,
};
