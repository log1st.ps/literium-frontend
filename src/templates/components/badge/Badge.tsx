import classNames from "classnames";
import React from "react";
import styles from "./badge.scss";

interface IBadge {
    state: "timer";
    title?: string;
    value?: string;
    isMobile?: boolean;
}

const Badge = ({
    state,
    title,
    value,
    isMobile,
}: IBadge) => (
    <div
        className={classNames({
            [styles.badge]: true,
            [styles.isMobile]: isMobile,
            [styles[`${state}State`]]: true,
        })}
    >
        <div className={styles.title}>{title}</div>
        <div className={styles.value}>{value}</div>
    </div>
);

export {
    Badge,
};
