import React from "react";
import {Widget} from "../../widget/Widget";

import classNames from "classnames";
import {Button} from "../../../elements/button/Button";
import styles from "./messageWidget.scss";

interface IMessageWidget {
    title?: string;
    message?: string;
    button?: {
        label: string;
        onClick?(): void;
    };
    isMobile?: boolean;
}

const MessageWidget = ({
    title,
    message,
    button,
    isMobile,
}: IMessageWidget) => (
    <Widget
        title={title}
        isMobile={isMobile}
    >
        <div
            className={classNames({
                [styles.row]: true,
                [styles.isMobile]: isMobile,
            })}
        >
            <div className={styles.message}>{message}</div>
            {button && (
                <div className={styles.button}>
                    <Button
                        state={"action"}
                        size={"small"}
                        onClick={button.onClick}
                    >
                        {button.label}
                    </Button>
                </div>
            )}
        </div>
    </Widget>
);

export {
    MessageWidget,
};
