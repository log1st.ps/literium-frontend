import classNames from "classnames";
import React, {forwardRef, RefObject} from "react";
import {compose, withProps} from "recompose";

import {ReactComponentLike} from "prop-types";
import {withForwardedRef} from "../../../hoc/withForwardedRef";
import styles from "./icon.scss";

export const iconTypes =
    [
    "angleBottom"
    , "angleLeft"
    , "check"
    , "clock"
    , "fileCSV"
    , "fileJSON"
    , "files"
    , "fileXML"
    , "gear"
    , "hamburger"
    , "home"
    , "lifebuoy"
    , "notifications"
    , "paycheck"
    , "questionMark"
    , "signOut"
    , "tiles"
    , "wallet"
    , "walletIn"
    , "walletPlus"
    , "exclamationPoint"
    , "plus"
    , "arrowDown"
    , "magnifier"
    , "date"
    , "plusMinus"
    , "question"
    , "link"
    , "commentCheck"
    , "trash"
    , "oval"
    , "dots"
    , "refresh"
    , ] as const;

export const iconSizes =
    [
    "lt"
    , "xs"
    , "sm"
    , "xsm"
    , "md"
    , "lg"
    , "xl"
    , ] as const;

interface IIconInner extends IIcon {
    IconComponent: ReactComponentLike;
    forwardedRef?: RefObject<any>;
}

const IconInner = ({
    IconComponent,
    size,
    forwardedRef,
}: IIconInner) => (
    <div
        className={classNames({
            [styles.icon]: true,
            [styles[size]]: !!size,
        })}
        ref={forwardedRef}
    >
        <IconComponent/>
    </div>
);

export interface IIcon {
    type: typeof iconTypes[number];
    size?: typeof iconSizes[number];
}

const Icon = compose<IIcon, IIcon>(
    withProps<IIconInner, IIcon>(({
        type,
    }) =>
        ({
            IconComponent: require(`./assets/${type}.inline.svg`).default as any,
            type,
        })),
    withForwardedRef(),
)(IconInner);

export {
    Icon,
};
