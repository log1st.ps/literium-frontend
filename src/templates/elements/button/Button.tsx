import classNames from "classnames";
import {ReactNode} from "react";
import {compose, withHandlers} from "recompose";
import {renderSlot} from "../../../helpers/renderSlot";
import {withForwardedRef} from "../../../hoc/withForwardedRef";
import styles from "./button.scss";

export interface IButton {
    children?: ReactNode;
    size?: "small" | "default" | "medium" | "big" | string;
    state?: "action" | "default" | "outline" | "outlineLight" | "secondary" | "pure" | "secondaryPure" | "tertiaryPure" | string;
    renderBefore?: ReactNode;
    renderAfter?: ReactNode;
    isBlock?: boolean;
    isOneLine?: boolean;

    forwardedRef?: any;

    onClick?(): void;
}

const InnerButton = ({
    children,
    size = "default",
    state = "default",
    renderBefore,
    renderAfter,
    onClick,
    isBlock,
    isOneLine,
    forwardedRef,
}: IButton) => (
    <button
        ref={forwardedRef}
        className={classNames({
            [styles.button]: true,
            [styles.isBlock]: isBlock,
            [styles.isOneLine]: isOneLine,
            [styles[`${size}Size`]]: true,
            [styles[`${state}State`]]: true,
        })}
        onClick={onClick}
    >
        {renderBefore && (
            <span className={styles.before}>
                {renderSlot(renderBefore)}
            </span>
        )}
        {children && (
            <span className={styles.label}>
                {renderSlot(children)}
            </span>
        )}
        {renderAfter && (
            <span className={styles.after}>
                {renderSlot(renderAfter)}
            </span>
        )}
    </button>
);

const Button = compose<any, IButton>(
    withForwardedRef(),
    withHandlers({
        onClick: ({onClick}) => (e) => {
            if (onClick) {
                onClick(e);
            }
        },
    }),
)(InnerButton);

export {
    Button,
};
