import Link, {LinkProps} from "next/link";
import { useRouter} from "next/router";
import React, {Children} from "react";

interface IActiveLink extends LinkProps {
    activeClassName: string;
    children?: any;
}

export const ActiveLink = ({
    activeClassName,
    ...props
}: IActiveLink) => {
    return (
        <Link {...props}>
            {React.cloneElement(Children.only(props.children), {
                className: [
                    Children.only(props.children).props.className,
                    useRouter().pathname === props.href && activeClassName,
                ].filter(Boolean).join(" "),
            })}
        </Link>
    );
};
