import classNames from "classnames";
import React, {ReactNode} from "react";
import {compose, withProps} from "recompose";
import {calculateGridGap} from "../../../helpers/calculateGridGap";
import {renderSlot} from "../../../helpers/renderSlot";
import {withForwardedRef} from "../../../hoc/withForwardedRef";
import styles from "./grid.scss";

interface IGrid {
    cols?: number | Array<number | string>;
    gap?: number;
    align?: "start" | "center" | "end";
    children?: ReactNode;
}

interface IRow {
    direction?: "column" | "row";
    align?: "start" | "center" | "end";
    justify?: "start" | "center" | "end";
    gap?: number;
    children?: ReactNode;
}

interface ICol {
    direction?: "column" | "row";
    gap?: number;
    isGrow?: boolean;
    isShrink?: boolean;
    children?: ReactNode;
}

interface IBound {
    top?: number | string;
    left?: number | string;
    right?: number | string;
    bottom?: number | string;
    children?: ReactNode;
}

interface IPosition {
    children?: ReactNode;
    forwardedRef?: React.RefObject<any>;
    isRelative?: boolean;
    isAbsolute?: boolean;
    isFixed?: boolean;
    top?: number | string;
    left?: number | string;
    right?: number | string;
    bottom?: number | string;
    parentTop?: number | string;
    parentLeft?: number | string;
    parentRight?: number | string;
    parentBottom?: number | string;
    xAlign?: "left" | "center" | "right";
    yAlign?: "top" | "center" | "bottom";
    zIndex?: number;
}

const Grid = ({
    cols,
    gap,
    children,
    align,
}: IGrid) => (
    <div
        className={classNames({
            [styles.grid]: true,
            [styles[`${align}Align`]]: !!align,
        })}
        style={{
            gridTemplateColumns: !cols ? undefined :
                typeof cols === "number"
                    ? `repeat(${cols}, 1fr)`
                    : cols.map((item) => typeof item === "number" ? `${item}fr` : item).join(" "),
            gridGap: gap,
        }}
    >
        {renderSlot(children)}
    </div>
);

const Row = ({
    direction= "row",
    gap,
    align,
    justify,
    children,
}: IRow) => (
    <div
        className={classNames({
            [styles.row]: true,
            [styles[`${direction}Direction`]]: true,
            [styles[`${align}Align`]]: true,
            [styles[`${justify}Justify`]]: true,
        })}
        style={calculateGridGap({direction, gap: -gap})}
    >
        {children}
    </div>
);

const Col = ({
    direction= "row",
    gap,
    isShrink,
    isGrow,
    children,
}: ICol) => (
    <div
        className={styles.col}
        style={{
            flexShrink: typeof isShrink === "boolean" && (isShrink ? 1 : 0),
            flexGrow: typeof isGrow === "boolean" && (isGrow ? 1 : 0),
            ...calculateGridGap({direction, gap}),
        }}
    >
        {children}
    </div>
);

const calculateBounds = (props: IBound, attribute = null) => ({
    style: {
        ...["top", "right", "bottom", "left"].reduce((prev, key) => ({
            ...prev,
            [`${attribute || ""}${attribute ? (key[0].toUpperCase() + key.substr(1)) : key}`]: props[key],
        }), {}),
    },
});

const Margin = ({
    children,
    ...props
}: IBound) => (
    <div {...calculateBounds(props, "margin")}>
        {renderSlot(children)}
    </div>
);

const Padding = ({
    children,
    ...props
}: IBound) => (
    <div {...calculateBounds(props, "padding")}>
        {renderSlot(children)}
    </div>
);

const Position = compose<any, IPosition>(
    withForwardedRef(),
    withProps(({isRelative, isAbsolute, isFixed}) => ({
        isRelative: typeof isRelative === "undefined" ? (!(isAbsolute || isFixed)) : isRelative,
    })),
)(
    ({
        children,
        forwardedRef,
        isRelative,
        isAbsolute,
        isFixed,
        xAlign,
        yAlign,
        zIndex,
        parentBottom,
        parentLeft,
        parentRight,
        parentTop,
        ...props
    }: IPosition) => (
        <div
            className={classNames({
                [styles.positionAbsolute]: isAbsolute,
                [styles.positionFixed]: isFixed,
                [styles.positionRelative]: isRelative,
                [styles[`${xAlign}XAlign`]]: !!xAlign,
                [styles[`${yAlign}YAlign`]]: !!yAlign,
            })}
            ref={forwardedRef}
            {...calculateBounds({top: parentTop, left: parentLeft, right: parentRight, bottom: parentBottom})}
        >
            <div
                className={classNames({
                    [styles.positionChildren]: isAbsolute || isFixed || isRelative,
                })}
                style={{
                    ...(calculateBounds(props).style),
                    zIndex,
                }}
            >
                {children}
            </div>
        </div>
    ),
);

export {
    Grid,
    Row,
    Col,
    Margin,
    Padding,
    Position,
};
