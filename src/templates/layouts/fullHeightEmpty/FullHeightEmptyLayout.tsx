import classNames from "classnames";
import {compose, lifecycle} from "recompose";
import {renderSlot} from "../../../helpers/renderSlot";
import {withMobileObserver} from "../../../hoc/withMobileObserver";
import styles from "./fullHeightEmptyLayout.scss";

const Inner = ({
    children,
    isMobile,
}) => (
    <div
        className={classNames({
            [styles.layout]: true,
            [styles.isMobile]: isMobile,
        })}
    >
        {renderSlot(children)}
    </div>
);

const FullHeightEmptyLayout = compose<any, any>(
    withMobileObserver,
    lifecycle({
        componentDidMount() {
            document.documentElement.classList.add("fullHeightEmptyLayout");
        },
        componentWillUnmount() {
            document.documentElement.classList.remove("fullHeightEmptyLayout");
        },
    }),
)(Inner);

export {
    FullHeightEmptyLayout,
};
