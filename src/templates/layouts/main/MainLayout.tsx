import classNames from "classnames";
import React, {ReactNode} from "react";
import {compose, lifecycle} from "recompose";
import {renderSlot} from "../../../helpers/renderSlot";
import styles from "./mainLayout.scss";

export interface IMainLayout {
    renderHeader?: ReactNode;
    renderSide?: ReactNode;
    children?: ReactNode;
    isMobile?: boolean;
}

const MainLayoutInner = ({
    renderSide,
    renderHeader,
    children,
    isMobile,
}: IMainLayout) => (
    <div
        className={classNames({
            [styles.layout]: true,
            [styles.isMobile]: isMobile,
        })}
    >
        {(renderSide && !isMobile) && (
            <div className={styles.side}>
                {renderSlot(renderSide)}
            </div>
        )}
        <div className={styles.content}>
            {renderHeader && (
                <div className={styles.header}>
                    {renderSlot(renderHeader)}
                </div>
            )}
            <div className={styles.body}>
                {children}
            </div>
        </div>
    </div>
);

const MainLayout = compose<IMainLayout, IMainLayout>(
    lifecycle({
        componentDidMount() {
            document.documentElement.classList.add(styles.root);
        },
        componentWillUnmount() {
            document.documentElement.classList.remove(styles.root);
        },
    }),
)(MainLayoutInner);

export {
    MainLayout,
};
