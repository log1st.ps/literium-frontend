import classNames from "classnames";
import React, {ReactNode} from "react";
import {renderSlot} from "../../../../helpers/renderSlot";
import styles from "./abstractSignUpForm.scss";

interface IAbstractSignUpForm {
    imageType?: "blocked" | "encrypt" | "import" | "login" | "password" | "password2" | "passwordPhrase";
    children?: ReactNode;
    isMobile?: boolean;
}

const AbstractSignUpForm = ({
    imageType,
    children,
    isMobile,
}: IAbstractSignUpForm) => (
    <div
        className={classNames({
            [styles.layout]: true,
            [styles.isMobile]: isMobile,
        })}
    >
        {!isMobile && (
            <div className={styles.imageContainer}>
                <div
                    className={classNames({
                        [styles.image]: true,
                        [styles[`${imageType}Type`]]: true,
                    })}
                    style={{backgroundImage: `url(${require(`./assets/${imageType}.png`)})`}}
                />
            </div>
        )}
        <div className={styles.form}>
            {renderSlot(children)}
        </div>
    </div>
);

export {
    AbstractSignUpForm,
};
