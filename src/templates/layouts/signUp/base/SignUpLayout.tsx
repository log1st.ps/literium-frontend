import classNames from "classnames";
import React, {Fragment, ReactNode} from "react";
import {compose} from "recompose";
import {renderSlot} from "../../../../helpers/renderSlot";
import styles from "./signUpLayout.scss";

interface ISignUpLayout {
    renderLogo?: ReactNode;
    renderSideActions?: ReactNode;
    renderTitle?: ReactNode;
    renderActions?: ReactNode;
    imageType?: ReactNode;
    renderHint?: ReactNode;
    renderPreloader?: ReactNode;
    children?: ReactNode;
    withBackground?: boolean;
    isMobile?: boolean;
    isForm?: boolean;
}

interface IInnerSignUpLayout extends ISignUpLayout {

}

export const availableSignUpForms = ["phraseGeneration", "passwordCreation", "welcomeBack", "login", "blocked", "importWallet", "importWalletLast"] as const;
export const availableSignUpSteps = ["initial", "encrypting", "import", "loginSuccess", "created", "imported"] as const;

const renderLandingAndLoaders = ({
     renderActions,
     renderSideActions,
     renderTitle,
     isMobile,
     withBackground,
     imageType,
     renderHint,
     renderPreloader,
}: ISignUpLayout) => (
    <Fragment>
        {withBackground && (
            <div
                className={classNames({
                    [styles.background]: true,
                })}
            />
        )}
        <div className={styles.content}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderSlot(renderTitle)}
                </div>
            )}
            {renderActions && (
                <div className={styles.actions}>
                    {renderSlot(renderActions)}
                </div>
            )}
            {(isMobile && withBackground) && (
                <Fragment>
                    <div className={styles.divider}/>
                    {renderSideActions && (
                        <div className={styles.additionalActions}>
                            {renderSlot(renderSideActions)}
                        </div>
                    )}
                </Fragment>
            )}
            {imageType && (
                <div
                    className={classNames({
                        [styles.image]: true,
                        [styles[`${imageType}Image`]]: true,
                    })}
                    style={{
                        backgroundImage: `url(${require(`./assets/${imageType}${isMobile ? "Small" : ""}.png`)})`,
                    }}
                />
            )}
            {renderHint && (
                <div className={styles.hint}>
                    {renderSlot(renderHint)}
                </div>
            )}
            {renderPreloader && (
                <Fragment>
                    <div className={styles.preloaderIcon}>
                        {Array(3).fill(null).map((i, index) => (
                            <div key={index}/>
                        ))}
                    </div>
                    <div className={styles.preloader}>
                        {renderSlot(renderPreloader)}
                    </div>
                </Fragment>
            )}
        </div>
    </Fragment>
);

const InnerSignUpLayout = ({
    isForm,
    children,
    renderLogo,
    ...props
}: IInnerSignUpLayout) => (
    <div
        className={classNames({
            [styles.layout]: true,
            [styles.withoutBackground]: !props.withBackground,
            [styles.isMobile]: props.isMobile,
        })}
    >
        {(renderLogo) && (
            <div className={styles.header}>
                {renderLogo && (
                    <div className={styles.logo}>
                        {renderSlot(renderLogo)}
                    </div>
                )}
                {(!props.isMobile && props.renderSideActions) && (
                    <div className={styles.sideActions}>
                        {renderSlot(props.renderSideActions)}
                    </div>
                )}
            </div>
        )}
        {!isForm ? renderLandingAndLoaders(props) : (
            <div className={styles.form}>
                {renderSlot(children)}
            </div>
        )}
    </div>
);

const SignUpLayout = compose<IInnerSignUpLayout, IInnerSignUpLayout>(

)(InnerSignUpLayout);

export {
    SignUpLayout,
};
