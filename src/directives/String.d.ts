// tslint:disable-next-line:interface-name
interface String {
    format(...arguments: [{[key: string]: string}]): string;

    messageFormat(args: {[key: string]: any}): string;
}
