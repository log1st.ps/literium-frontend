import {Reducer} from "redux";
import {createReducer} from "../../helpers/createReducer";
import {ITranslation} from "../../interfaces/ITranslation";

export interface II18nState {
    translation: ITranslation;
}

const i18nState: II18nState = {
    translation: require("../../languages/en").default,
};

const reducer: Reducer = createReducer<II18nState>(
    i18nState, {
        setLanguage: (state, payload) => ({
            ...state,
            translation: require(`../../languages/${payload}`).default,
        }),
    },
);

export {
    reducer,
};
