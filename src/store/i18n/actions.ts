export const setLanguage = (language: string) => ({
    type: "setLanguage",
    payload: language,
});
