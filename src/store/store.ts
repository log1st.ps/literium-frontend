import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import {isDevelopment} from "../config";
import {IGlobalState, reducer as global} from "./global/reducer";
import {II18nState, reducer as i18n} from "./i18n/reducer";
import {INotificationsState, reducer as notifications} from "./notifications/reducer";
import {IUserState, reducer as user} from "./user/reducer";

const reducer = combineReducers({
    global,
    user,
    notifications,
    i18n,
});

export interface IState {
    global: IGlobalState;
    user: IUserState;
    notifications: INotificationsState;
    i18n: II18nState;
}

const composeEnhancers =
    typeof window === "object" && isDevelopment && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
        : compose;

export const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(
        thunk,
    )),
);
