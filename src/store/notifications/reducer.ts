import {Reducer} from "redux";
import {createReducer} from "../../helpers/createReducer";

export interface INotificationsState {
    count: number;
}

const notificationsState: INotificationsState = {
    count: 2,
};

const reducer: Reducer = createReducer<INotificationsState>(
    notificationsState, {
        setNotificationsCount: (state, payload) => ({
            ...state,
            notificationsCount: payload,
        }),
    },
);

export {
    reducer,
};
