export const setNotificationsCount = (notificationsCount: number) => ({
    type: "setNotificationsCount",
    payload: notificationsCount,
});
