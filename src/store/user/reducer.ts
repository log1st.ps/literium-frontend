import {Reducer} from "redux";
import {createReducer} from "../../helpers/createReducer";
import {IUser} from "../../interfaces/IUser";

export interface IUserState {
    user: IUser;
}

const userState: IUserState = {
    user: {
        avatarUrl: require("../../tmp/images/avatar.png"),
        firstName: "Samantha",
    },
};

const reducer: Reducer = createReducer<IUserState>(
    userState, {
        setUser: (state, payload) => ({
            ...state,
            user: payload,
        }),
    },
);

export {
    reducer,
};
