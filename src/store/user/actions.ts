import {IUser} from "../../interfaces/IUser";

export const setUser = (user: IUser) => ({
    type: "setUser",
    payload: user,
});
