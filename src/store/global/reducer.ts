import {Reducer} from "redux";
import {createReducer} from "../../helpers/createReducer";

export interface IGlobalState {
    pageTitle: string;
    isMobile: boolean;
}

const initialState: IGlobalState = {
    pageTitle: null,
    isMobile: false,
};

export const reducer: Reducer = createReducer<IGlobalState>(initialState, {
    setPageTitle: (state, payload) => ({
        ...state,
        pageTitle: payload,
    }),
    setIsMobile: (state, payload) => ({
        ...state,
        isMobile: payload,
    }),
});
