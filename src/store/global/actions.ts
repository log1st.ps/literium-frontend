export const setPageTitle = (pageTitle: string) => ({
    type: "setPageTitle",
    payload: pageTitle,
});

export const setIsMobile = (isMobile: string) => ({
    type: "setIsMobile",
    payload: isMobile,
});
