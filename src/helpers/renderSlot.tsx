import React, {ReactNode} from "react";

export const renderSlot = (Render?: any, fallback?: string | ReactNode) => {
    return (
        Render ? (
            typeof Render === "function" ? <Render/> : Render
        ) : fallback
    );
};
