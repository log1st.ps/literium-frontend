
export const createReducer = <U>(
    initialState: U,
    actions: {[key: string]: ((state, {type, payload}) => void)},
) =>
    (state = initialState, {type, payload}) => ({
        ...state,
        ...(type in actions ? (actions[type](state, payload)) : {}),
    });
