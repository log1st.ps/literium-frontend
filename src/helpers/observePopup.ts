interface IObservePopup {
    target: HTMLElement;
    popup: HTMLElement;
    arrow: HTMLElement;
    hide?(): void;
    onOutsideClick?(): void;
    safeOutsideElements?: HTMLElement[];

    isFixed?: boolean;
    isAbsolute?: boolean;

    offset?: number;
    otherOffset?: number;

    position?: "top" | "left" | "center" | "right" | "bottom";
    align?: "start" | "center" | "end";

    forceLeft?: number;
    forceTop?: number;
    forceRight?: number;
    forceBottom?: number;
    forceHeight?: number | HTMLElement;
    forceWidth?: number | HTMLElement;

    isHeightSticky?: boolean;
    isWidthSticky?: boolean;

    isSticky?: boolean;
    stickyPos?: number;
    stickyOtherPos?: number;
}

export interface IObservePopupInstance {
    destroy(): void;
    calculate(): void;
    deferCalculate(): void;
    hide?(): void;
    hideOther?(): void;
    onOutsideClick?(): void;
    align: IObservePopup["align"];
    position: IObservePopup["position"];
    offset: IObservePopup["offset"];
    otherOffset: IObservePopup["otherOffset"];
    stickyPos: IObservePopup["stickyPos"];
    stickyOtherPos: IObservePopup["stickyOtherPos"];
    isSticky: IObservePopup["isSticky"];
    forceLeft: IObservePopup["forceLeft"];
    forceTop: IObservePopup["forceTop"];
    forceRight: IObservePopup["forceRight"];
    forceBottom: IObservePopup["forceBottom"];
    forceWidth: IObservePopup["forceWidth"];
    forceHeight: IObservePopup["forceHeight"];
    popup: HTMLElement;
    target: HTMLElement;
    safeOutsideElements: HTMLElement[];

    isHeightSticky?: boolean;
    isWidthSticky?: boolean;
}

const observers: IObservePopupInstance[] = [];

const handleObservers = () => {
    observers.forEach((observer) => {
        observer.calculate();
    });
};

const handleOutsideClick = (e: {target: HTMLElement}) => {
    const {target} =  e;
    observers.forEach((observer) => {
        if (!observer.onOutsideClick) {
            return;
        }

        let isSafe = false;
        [observer.target, ...Array.from(observer.popup.children), ...observer.safeOutsideElements].filter((item) => {
            if (isSafe) {
                return;
            }

            if (item === target || item.contains(target)) {
                isSafe = true;
            }
        });

        if (isSafe) {
            return;
        }

        observer.onOutsideClick();
    });
};

export const handlePopupObservers = ({
    scrollObject = window,
}: {scrollObject: any} = {scrollObject: window}) => {
    window.addEventListener("resize", handleObservers);
    scrollObject.addEventListener("scroll", handleObservers);
    handleObservers();

    document.addEventListener("click", handleOutsideClick as any);
};

export const observePopup = ({
    target,
    popup,
    arrow,
    hide,
    isFixed,
    isAbsolute,
    offset = 0,
    otherOffset = 0,
    position= "bottom",
    align= "start",
    isSticky,
    stickyPos= 0,
    stickyOtherPos= 0,
    forceLeft,
    forceTop,
    forceRight,
    forceBottom,
    forceHeight,
    forceWidth,
    onOutsideClick,
    safeOutsideElements = [],
    isHeightSticky,
    isWidthSticky,
}: IObservePopup): IObservePopupInstance => {
    if (isFixed) {
        popup.style.position = "fixed";
    }
    if (isAbsolute) {
        popup.style.position = "absolute";
    }

    const observer = {
        hide,
        align,
        position,
        offset,
        otherOffset,
        stickyPos,
        stickyOtherPos,
        isSticky,
        forceLeft,
        forceTop,
        forceRight,
        forceBottom,
        forceHeight,
        forceWidth,
        popup,
        target,
        onOutsideClick,
        safeOutsideElements,
        isWidthSticky,
        isHeightSticky,
        hideOther: () => {
            observers
                .filter((obs) => obs !== observer)
                .forEach((obs) => {
                    if (obs.hide) {
                        obs.hide();
                    }
                });
        },
        destroy() {
            observers.splice(
                observers.findIndex((obs) => obs = observer),
                1,
            );
        },
        deferCalculate() {
            setTimeout(() => {
                observer.calculate();
            });
        },
        calculate() {
            const {
                top: targetTop,
                left: targetLeft,
            } = target.getBoundingClientRect();

            const bounds = {
                top: targetTop,
                left: targetLeft,
                bottom: undefined,
                right: undefined,
            };

            const current = ["top", "center", "bottom"].indexOf(observer.position) > 1 ? "top" : "left";
            const next = ["top", "center", "bottom"].indexOf(observer.position) > -1 ? ["left", "clientWidth"] : ["top", "clientHeight"];

            bounds[current] += observer.offset;
            bounds[next[0]] += observer.otherOffset;

            if (observer.position === "left") { bounds.left -= popup.clientWidth; }
            if (observer.position === "center") { bounds.left -= target.clientWidth / 2; }
            if (observer.position === "right") { bounds.left += target.clientWidth; }
            if (observer.position === "bottom") { bounds.top += target.clientHeight; }
            if (observer.position === "top") { bounds.top -= popup.clientHeight; }
            if (observer.align === "center") { bounds[next[0]] += (target[next[1]] - popup[next[1]]) / 2; }
            if (observer.align === "end") { bounds[next[0]] += target[next[1]] - popup[next[1]]; }

            if (observer.isSticky) {
                if (bounds[next[0]] < observer.stickyPos) {
                    bounds[next[0]] = observer.stickyPos;
                }
                if (bounds[current] < observer.stickyOtherPos) {
                    bounds[current] = observer.stickyOtherPos;
                }
            }

            ["left", "top", "right", "bottom"].map((i) => {
                const force = observer[`force${i[0].toUpperCase() + i.substr(1)}`];
                if (force !== undefined) {
                    bounds[i] = force;
                }
            });

            popup.style.left = `${bounds.left}px`;
            popup.style.top = `${bounds.top}px`;

            if (observer.forceWidth) {
                popup.style.width = `${
                    typeof observer.forceWidth === "number"
                        ? observer.forceWidth
                        : observer.forceWidth.offsetWidth
                }px`;
            }
            if (observer.forceHeight) {
                popup.style.height = `${
                    typeof observer.forceHeight === "number"
                        ? observer.forceHeight
                        : observer.forceHeight.offsetHeight
                }px`;
            }

            if (arrow) {
                const arrowOffset =
                    target.getBoundingClientRect()[next[0]]
                    - popup.getBoundingClientRect()[next[0]]
                    + target[next[1]] / 2;
                arrow.style[next[0]] = `${arrowOffset}px`;
            }

            const {
                right,
                bottom,
                height,
                width,
            } = popup.getBoundingClientRect();

            if (isHeightSticky) {
                const differ = height + window.innerHeight - bottom;
                popup.style.height = `${differ}px`;
            }   else {
                popup.style.height = "none";
            }

            if (isWidthSticky) {
                const differ = height + window.innerWidth - right;
                popup.style.width = `${differ}px`;
            }   else {
                popup.style.width = "none";
            }
        },
    };

    observers.push(observer);
    observer.deferCalculate();

    return observer;
};
