export const getRandomString = (length: number = 7) =>
    Math.random().toString(36).substring(length);
