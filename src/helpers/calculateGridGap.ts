const calculateGridGap = ({direction, gap}) => ({
    [`margin${["row", "rowReverse"].indexOf(direction) > -1 ? "Left" : "Top"}`]: gap ? gap / 2 : undefined,
    [`margin${["row", "rowReverse"].indexOf(direction) > -1 ? "Right" : "Bottom"}`]: gap ? gap / 2 : undefined,
});

export {
    calculateGridGap,
};
