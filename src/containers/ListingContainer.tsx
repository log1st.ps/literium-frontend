import {ReactComponentLike} from "prop-types";
import React, {createRef, Fragment} from "react";
import {compose, lifecycle, withHandlers, withProps, withState} from "recompose";
import {withForwardedRef} from "../hoc/withForwardedRef";
import {withTranslation} from "../hoc/withTranslation";
import {ITranslation} from "../interfaces/ITranslation";
import {ButtonsList, IButtonsList} from "../templates/components/buttonsList/ButtonsList";
import {Display} from "../templates/components/display/Display";
import {Divider} from "../templates/components/divider/Divider";
import {Checkbox} from "../templates/components/form/elements/checkbox/Checkbox";
import {Datepicker} from "../templates/components/form/elements/datepicker/Datepicker";
import {IFormField} from "../templates/components/form/elements/IFormField";
import {Input} from "../templates/components/form/elements/input/Input";
import {Form} from "../templates/components/form/Form";
import {Popup} from "../templates/components/popup/Popup";
import {Styler} from "../templates/components/styler/Styler";
import {ITableAction, ITableColumn, ITableData, ITableSlots, Table} from "../templates/components/table/Table";
import {H6, P} from "../templates/components/typography/Typography";
import {Widget} from "../templates/components/widget/Widget";
import {Icon, IIcon} from "../templates/elements/icon/Icon";
import {Col, Grid, Margin, Padding, Position, Row} from "../templates/layouts/grid/Grid";

// @TODO: Decomposition after accomplishment needed

interface IFilter {
    type: "input" | "checkbox" | "datepicker";
    label: string;
    icon?: IIcon["type"];
    value?: any;
    config?: IFormField;
}

interface IListingContainer extends ITableSlots {
    modelName: "Transactions";
    sortAttributes?: {
        [key: string]: {
            label: string;
            isCheckbox?: boolean;
            icon?: IIcon["type"];
            sorts?: Array<"asc" | "desc">;
        };
    };
    filterConfig?: {
        [key: string]: IFilter,
    };
    isMobile?: boolean;
    primaryActions?: IButtonsList["buttons"];

    columns?: string[] | ITableColumn[];
    data?: ITableData[];
    rowActions?: ITableAction[];
}

interface IInnerListingContainer extends IListingContainer {
    texts: ITranslation["listing"];
    children: any;

    filterModel: {
        [key: string]: any,
    };

    filterConfig: {
        [key: string]: IFilter & {
            Component: ReactComponentLike,
        },
    };

    setFilterField(field: string): (value: any) => void;

    isSortPopupActive: boolean;
    setIsSortPopupActive(isSortPopupActive: boolean): void;
    toggleIsSortPopupActive(): void;

    isFilterPopupActive: boolean;
    setIsFilterPopupActive(isFilterPopupActive: boolean): void;
    toggleIsFilterPopupActive(): void;

    isSearchInputActive: boolean;
    setIsSearchInputActive(isSearchInputActive: boolean): void;
    showSearchInput(): void;
    hideSearchInput(): void;

    searchValue: string;
    setSearchValue(searchValue): string;

    contentRef: React.RefObject<any>;
    sortByRef: React.RefObject<any>;
    sortByTriggerRef: React.RefObject<any>;
    filterByRef: React.RefObject<any>;
    filterByTriggerRef: React.RefObject<any>;
}

const ConfigButton = withForwardedRef<any>()(({
    label,
    forwardedRef,
    onClick = null,
    isMobile,
    isSearchInputActive,
}) => (
    <div ref={forwardedRef}>
        <Padding
            left={(isMobile && isSearchInputActive) ? 8 : 31}
            right={(isMobile && isSearchInputActive) ? 8 : 31}
        >
            <ButtonsList
                justify={"end"}
                buttons={[{
                    key: "sort",
                    label,
                    state: "tertiaryPure",
                    onClick,
                    renderAfter: (
                        <div>
                            <Icon type={"arrowDown"} size={"xs"}/>
                        </div>
                    ),
                }]}
            />
        </Padding>
    </div>
));

const Inner = ({
    modelName,
    sortAttributes = {},

    texts,

    filterConfig,
    filterModel,
    setFilterField,

    isSortPopupActive,
    toggleIsSortPopupActive,

    isFilterPopupActive,
    toggleIsFilterPopupActive,

    isMobile,

    isSearchInputActive,
    showSearchInput,
    hideSearchInput,

    searchValue,
    setSearchValue,

    primaryActions,

    columns,
    data,
    rowActions,

    renderHeader,
    renderHeaderCell,
    renderRow,
    renderRowCell,
    renderRowCellInner,

    onRowClick,

    contentRef,
    sortByRef,
    sortByTriggerRef,
    filterByRef,
    filterByTriggerRef,
}: IInnerListingContainer) => (
    <Fragment>
        <Position ref={contentRef}>
            <Grid gap={isMobile ? 28 : 16}>
                <Grid align={"center"} gap={30} cols={isMobile ? 1 : ["1fr", "auto"]}>
                    <Styler height={40}>
                        {primaryActions && (
                            <ButtonsList
                                buttons={primaryActions}
                            />
                        )}
                    </Styler>
                    <Position zIndex={10} isRelative={isMobile}>
                        <Row
                            gap={isMobile ? (!isSearchInputActive ? 62 : 16) : 0}
                            justify={isMobile ? "start" : "end"}
                            align={"center"}
                        >
                            <Position isRelative={isMobile ? false : undefined}>
                                <ConfigButton
                                    label={texts.sortBy}
                                    onClick={toggleIsSortPopupActive}
                                    isMobile={isMobile}
                                    isSearchInputActive={isSearchInputActive}
                                    ref={sortByTriggerRef}
                                />
                                <Display
                                    visibility={isSortPopupActive ? "visible" : "hidden"}
                                    opacity={isSortPopupActive ? 1 : 0}
                                >
                                    <Position
                                        isAbsolute={true}
                                        xAlign={isMobile ? undefined : "center"}
                                        yAlign={isMobile ? undefined : "bottom"}
                                        top={13}
                                        left={isMobile ? 0 : (62 / 2 - 8)}
                                        right={isMobile ? 0 : undefined}
                                        parentLeft={isMobile ? 0 : undefined}
                                        parentRight={isMobile ? 0 : undefined}
                                    >
                                        <Popup
                                            overflowScroll={true}
                                            width={isMobile ? `100%` : 260}
                                            arrowType={"top"}
                                            title={texts.sortBy}
                                            arrowLeft={isMobile ? 61 : "calc(50% + 4px)"}
                                            ref={{
                                                content: sortByRef,
                                            } as any}
                                        >
                                            <Margin bottom={-20}>
                                                {Object.entries(sortAttributes).map(([key, sort]) => (
                                                    <Fragment key={key}>
                                                        <Divider isDark={true}/>
                                                        <Padding top={20} bottom={20}>
                                                            <Grid gap={12} cols={["24px", "auto"]}>
                                                                <Col>
                                                                    {sort.icon && (
                                                                        <Icon type={sort.icon}/>
                                                                    )}
                                                                </Col>
                                                                <Grid gap={9}>
                                                                    <H6 color={"dark"} isBold={true}>{sort.label}</H6>
                                                                    {sort.sorts && sort.sorts.map((sortType) => (
                                                                        <P
                                                                            key={sortType}
                                                                            color={"dark"}
                                                                        >
                                                                            {texts.sort[sortType]}
                                                                        </P>
                                                                    ))}
                                                                </Grid>
                                                            </Grid>
                                                        </Padding>
                                                    </Fragment>
                                                ))}
                                            </Margin>
                                        </Popup>
                                    </Position>
                                </Display>
                            </Position>
                            <Divider height={16} width={1}/>
                            <Position isRelative={isMobile ? false : undefined}>
                                <ConfigButton
                                    label={texts.filterBy}
                                    onClick={toggleIsFilterPopupActive}
                                    isMobile={isMobile}
                                    isSearchInputActive={isSearchInputActive}
                                    ref={filterByTriggerRef}
                                />
                                <Display
                                    visibility={isFilterPopupActive ? "visible" : "hidden"}
                                    opacity={isFilterPopupActive ? 1 : 0}
                                >
                                    <Position
                                        isAbsolute={true}
                                        xAlign={isMobile ? undefined : "center"}
                                        yAlign={isMobile ? undefined : "bottom"}
                                        top={13}
                                        left={isMobile ? 0 : (62 / 2 - 8)}
                                        right={isMobile ? 0 : undefined}
                                        parentLeft={isMobile ? 0 : undefined}
                                        parentRight={isMobile ? 0 : undefined}
                                    >
                                        <Popup
                                            overflowScroll={true}
                                            width={isMobile ? `calc(100vw - 40px)` : 260}
                                            title={texts.filterBy}
                                            arrowType={"top"}
                                            arrowLeft={isMobile ? (isSearchInputActive ? 153 : 200) : "calc(50% + 6px)"}
                                            ref={{
                                                content: filterByRef,
                                            } as any}
                                        >
                                            {
                                                Object.entries(filterConfig).map(([key, filter]) => (
                                                    <Fragment key={key}>
                                                        <Divider isDark={true}/>
                                                        <Padding top={20} bottom={20}>
                                                            <Grid gap={12} cols={["24px", "auto"]}>
                                                                <Col>
                                                                    {filter.icon && (
                                                                        <Icon type={filter.icon}/>
                                                                    )}
                                                                </Col>
                                                                <Grid gap={6}>
                                                                    <H6 color={"dark"} isBold={true}>{filter.label}</H6>
                                                                    <filter.Component
                                                                        value={filterModel[key]}
                                                                        onInput={setFilterField(key)}
                                                                        {...filter.config}
                                                                    />
                                                                </Grid>
                                                            </Grid>
                                                        </Padding>
                                                    </Fragment>
                                                ))
                                            }
                                            <Divider isDark={true}/>
                                        </Popup>
                                    </Position>
                                </Display>
                            </Position>
                            {!isMobile && (
                                <Divider height={16} width={1}/>
                            )}
                            <Margin
                                left={isMobile ? "auto" : 0}
                                right={isMobile ? (!isSearchInputActive ? 31 : 8) : 0}
                            >
                                <Padding left={isMobile ? 0 : 32}>
                                    {(isMobile && !isSearchInputActive) ? (
                                        <ButtonsList
                                            buttons={[{
                                                key: "searchTrigger",
                                                state: "pure",
                                                renderBefore: (
                                                    <Icon type={"magnifier"} size={"sm"}/>
                                                ),
                                                onClick: showSearchInput,
                                            }]}
                                        />
                                    ) : (
                                        <Form.Input
                                            placeholder={texts.search.messageFormat({model: modelName})}
                                            hasAppearance={false}
                                            isLight={true}
                                            before={(
                                                <Styler cursor={"pointer"}>
                                                    <Icon type={"magnifier"} size={"sm"}/>
                                                </Styler>
                                            )}
                                            width={isMobile ? "auto" : 164}
                                            onBlur={isMobile && hideSearchInput}
                                            onFocus={isMobile && showSearchInput}
                                            isOutsideFocused={isSearchInputActive}
                                            value={searchValue}
                                            onInput={setSearchValue}
                                        />
                                    )}
                                </Padding>
                            </Margin>
                        </Row>
                    </Position>
                </Grid>
                <Widget
                    type={"table"}
                    isMobile={isMobile}
                >
                    <Table
                        columns={columns}
                        data={data}
                        keyField={"id"}
                        isMobile={isMobile}
                        actions={rowActions}
                        renderHeader={renderHeader}
                        renderHeaderCell={renderHeaderCell}
                        renderRow={renderRow}
                        renderRowCell={renderRowCell}
                        renderRowCellInner={renderRowCellInner}
                        onRowClick={onRowClick}
                        footerActions={[{
                            key: "load",
                            state: "action",
                            renderBefore: (
                                <Icon type={"refresh"} size={"xs"}/>
                            ),
                            label: texts.load,
                            onClick: () => {
                                console.log("load more");
                            },
                        }]}
                    />
                </Widget>
            </Grid>
        </Position>
    </Fragment>
);

const ListingContainer = compose<any, IListingContainer>(
    withTranslation("listing"),
    withState("isSortPopupActive", "setIsSortPopupActive", false),
    withState("isFilterPopupActive", "setIsFilterPopupActive", false),
    withState("isSearchInputActive", "setIsSearchInputActive", false),
    withState("searchValue", "setSearchValue", ""),
    withProps(({filterConfig}) => ({
        filterConfig: filterConfig || {},
    })),
    withState("filterModel", "setFilterModel", ({filterConfig}) => (
        Object
            .entries((filterConfig) as IListingContainer["filterConfig"])
            .reduce((previous, [key, filter]) => ({
                ...previous,
                [key]: filter.value,
            }), {})
    )),
    withHandlers({
        setFilterField: ({setFilterModel, filterModel}) => (field) => (value) => {
            setFilterModel({
                ...filterModel,
                [field]: value,
            });
        },
    }),
    withProps(({filterConfig}: {filterConfig: IListingContainer["filterConfig"]}) => ({
        filterConfig: Object.entries(filterConfig).reduce((previous, [key, value]) => ({
            ...previous,
            [key]: {
                ...value,
                Component: {
                    datepicker: Datepicker,
                    input: Input,
                    checkbox: Checkbox,
                }[value.type],
            },
        }), {}),
    })),
    withProps(() => ({
        contentRef: createRef(),

        sortByTriggerRef: createRef(),
        sortByRef: createRef(),
        filterByTriggerRef: createRef(),
        filterByRef: createRef(),
    })),
    withHandlers({
        toggleIsSortPopupActive: ({setIsSortPopupActive, isSortPopupActive}) => () => {
            setIsSortPopupActive(!isSortPopupActive);
        },
        toggleIsFilterPopupActive: ({setIsFilterPopupActive, isFilterPopupActive}) => () => {
            setIsFilterPopupActive(!isFilterPopupActive);
        },
        showSearchInput: ({setIsSearchInputActive}) => () => {
            setIsSearchInputActive(true);
        },
        hideSearchInput: ({setIsSearchInputActive}) => () => {
            setIsSearchInputActive(false);
        },
    }),
    lifecycle({
        componentDidMount() {
            document.addEventListener("click", ({target}) => {

                const {
                    sortByRef: {current: sort},
                    sortByTriggerRef: {current: sortTrigger},
                    isSortPopupActive,
                    setIsSortPopupActive,
                    filterByRef: {current: filter},
                    filterByTriggerRef: {current: filterTrigger},
                    isFilterPopupActive,
                    setIsFilterPopupActive,
                } = this.props as IInnerListingContainer;

                if (isSortPopupActive) {
                    if (![sort, sortTrigger].filter((e) => e === target || e.contains(target)).length) {
                        setIsSortPopupActive(false);
                    }
                }

                if (isFilterPopupActive) {
                    if (![filter, filterTrigger].filter((e) => e === target || e.contains(target)).length) {
                        setIsFilterPopupActive(false);
                    }
                }
            });
        },
    }),
)(Inner);

ListingContainer.displayName = "ListingContainer";

export {
    ListingContainer,
};
