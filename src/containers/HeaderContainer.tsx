import {NextRouter, Router, useRouter, withRouter} from "next/router";
import {connect} from "react-redux";
import {compose, lifecycle, withProps, withState} from "recompose";
import {withMobileObserver} from "../hoc/withMobileObserver";
import {IState} from "../store/store";
import {Header} from "../templates/components/header/Header";

interface IRoute {
    href: string;
    label: string;
}

interface IHeaderContainer {
    routes: IRoute[];
    setRoutes?(routes: IRoute[]): void;
    router: NextRouter;
    pageTitle: string;
}

const HeaderContainer = compose<any, any>(
    withState("routes", "setRoutes", []),
    withProps(({routes, setRoutes}) => ({
        router: useRouter(),
        previousPage: routes[routes.length - 1],
        onBackButtonClick() {
            setTimeout(() => {
                const newRoutes = [...routes];
                newRoutes.pop();
                setRoutes(newRoutes);
            }, 0);
        },
    })),
    connect(
        (state: IState) => ({
            pageTitle: state.global.pageTitle,
            notificationsCount: state.notifications.count,
            user: state.user.user,
        }),
    ),
    lifecycle({
        componentDidMount() {
            Router.events.on("routeChangeStart", () => {
                const {router, routes, setRoutes, pageTitle} = this.props as IHeaderContainer;

                setRoutes([
                    ...routes,
                    {
                        href: router.pathname,
                        label: pageTitle,
                    },
                ]);
            });
        },
    }),
    withMobileObserver,
)(Header);

export {
    HeaderContainer,
};
