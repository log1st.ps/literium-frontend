import {NextRouter, useRouter} from "next/router";
import {ReactComponentLike} from "prop-types";
import React, {ReactNode} from "react";
import {compose, withHandlers, withProps} from "recompose";
import {withTranslation} from "../hoc/withTranslation";
import {ITranslation} from "../interfaces/ITranslation";
import {ISide, Side} from "../templates/components/side/Side";
import {Icon, IIcon} from "../templates/elements/icon/Icon";

interface ISideContainer extends ISide {
    texts?: ITranslation["side"];
}

const getIcon = (icon: IIcon["type"]): ReactComponentLike => () => (
    <Icon size={"sm"} type={icon}/>
);

const SideContainer = compose<ISideContainer, any>(
    withTranslation("side"),
    withProps(() => ({
        router: useRouter(),
    })),
    withHandlers({
        onLogoClick: ({router}: {router: NextRouter}) => () => {
            router.push("/");
        },
    }),
    withProps<ISide, ISideContainer>(
        ({texts}) => ({
            menuItems: [
                {
                    key: "dashboard",
                    Icon: getIcon("tiles"),
                    label: texts.dashboard,
                    route: "/",
                },
                {
                    key: "transactions",
                    Icon: getIcon("wallet"),
                    label: texts.transactions,
                    route: "/transactions",
                },
                {
                    key: "invoices",
                    Icon: getIcon("paycheck"),
                    label: texts.invoices,
                    route: "/invoices",
                },
                {
                    key: "addresses",
                    Icon: getIcon("home"),
                    label: texts.addresses,
                    route: "/addresses",
                },
                {
                    key: "settings",
                    Icon: getIcon("gear"),
                    label: texts.settings,
                    route: "/settings",
                },
                "delimiter",
                {
                    key: "help",
                    Icon: getIcon("lifebuoy"),
                    label: texts.help,
                    route: "/help",
                },
                {
                    key: "signOut",
                    Icon: getIcon("signOut"),
                    label: texts.signOut,
                    onClick: () => {
                        console.log("Sign out");
                    },
                },
            ],
        }),
    ),
)(Side);

export {
    SideContainer,
};
