import {ReactComponentLike} from "prop-types";
import React from "react";
import {compose, withHandlers, withProps, withState} from "recompose";
import {withMobileObserver} from "../../hoc/withMobileObserver";
import {withTranslation} from "../../hoc/withTranslation";
import {ITranslation} from "../../interfaces/ITranslation";
import {ButtonsList} from "../../templates/components/buttonsList/ButtonsList";
import {Card} from "../../templates/components/card/Card";
import {Divider} from "../../templates/components/divider/Divider";
import {Form} from "../../templates/components/form/Form";
import {Img} from "../../templates/components/image/Img";
import {Modal} from "../../templates/components/modal/Modal";
import {H3, H5, H6, P} from "../../templates/components/typography/Typography";
import {Col, Grid, Margin, Padding, Row} from "../../templates/layouts/grid/Grid";
import {AbstractSignUpForm} from "../../templates/layouts/signUp/abstract/AbstractSignUpForm";

interface IPasswordCreationContainer {
    texts: ITranslation["pages"]["signUp"]["form"]["passwordCreation"];
    password: string;
    setPassword(password: string): void;
    passwordConfirmation: string;
    setPasswordConfirmation(passwordConfirmation: string): void;
    isMobile?: boolean;

    TitleComponent: ReactComponentLike;

    isModalActive: boolean;
    setIsModalActive(isModalActive: boolean): void;
    closeModal(): void;
    showModal(): void;
}

const Inner = ({
    texts,
    password,
    passwordConfirmation,
    setPassword,
    setPasswordConfirmation,
    isMobile,
    TitleComponent,
    isModalActive,
    showModal,
    closeModal,
}: IPasswordCreationContainer) => (
    <AbstractSignUpForm
        isMobile={isMobile}
        imageType={"encrypt"}
    >
        <Card isMobile={isMobile}>
            <Grid gap={isMobile ? 20 : 40}>
                <Grid gap={8}>
                    <TitleComponent color={"dark"} isBold={true}>
                        {
                            isMobile
                                ? texts.title.replace(/\n/g, " ")
                                : texts.title.split("\n").map((i, index) => (<div key={index}>{i}</div>))
                        }
                    </TitleComponent>
                    <P color={"grey"}>{texts.subTitle}</P>
                </Grid>
                <Form>
                    <Grid gap={20}>
                        <Form.Input
                            value={password}
                            onInput={setPassword}
                            placeholder={texts.password}
                            type={"password"}
                            isMobile={isMobile}
                            height={117}
                        />
                        <Form.Input
                            value={passwordConfirmation}
                            onInput={setPasswordConfirmation}
                            placeholder={texts.passwordConfirmation}
                            type={"password"}
                            isMobile={isMobile}
                            height={117}
                        />
                        <Row gap={isMobile ? 12 : 16} align={isMobile ? "start" : "center"}>
                            <Col gap={isMobile ? 12 : 16} isShrink={false}>
                                <Img
                                    url={require("../../assets/images/decryptionETA.svg")}
                                    height={isMobile ? 26 : 40}
                                    width={isMobile ? 24 : 40}
                                    type={"block"}
                                    backgroundType={"cover"}
                                    borderRadius={isMobile ? 12 : 20}
                                />
                            </Col>
                            <Col gap={isMobile ? 12 : 16}>
                                <P color={"grey"}>{texts.hint}</P>
                            </Col>
                        </Row>
                    </Grid>
                </Form>
                <Divider isDark={true} margin={isMobile && 8}/>
                <ButtonsList
                    gap={28}
                    justify={"end"}
                    direction={isMobile ? "columnReverse" : "row"}
                    buttons={[
                        {
                            key: "skip",
                            label: texts.skip,
                            state: "secondaryPure",
                            size: "medium",
                            isBlock: isMobile,
                            onClick: showModal,
                        },
                        {
                            key: "continue",
                            label: texts.continue,
                            state: "secondary",
                            size: "medium",
                            isBlock: isMobile,
                        },
                    ]}
                />
            </Grid>
        </Card>
        <Modal
            width={550}
            onClose={closeModal}
            isActive={isModalActive}
        >
            <Margin>
                <Card isMobile={isMobile}>
                    <Margin top={isMobile ? 40 : 20}>
                        <Row justify={"center"}>
                            <Img
                                url={require("../../assets/images/skip.png")}
                                height={130}
                                width={130}
                                borderRadius={65}
                                type={"block"}
                            />
                        </Row>
                    </Margin>
                    <Margin top={24}>
                        <H5 isBold={true} align={"center"} color={"dark"}>{texts.skipModal.title}</H5>
                    </Margin>
                    <Margin top={8}>
                        <P color={"grey"} align={"center"}>
                            {
                                isMobile
                                    ? texts.skipModal.description.replace(/\n/g, " ")
                                    : texts.skipModal.description
                                        .split("\n").map((i, index) => (<div key={index}>{i}</div>))
                            }
                        </P>
                    </Margin>
                    <Margin top={isMobile ? 30 : 40}>
                        <ButtonsList
                            gap={28}
                            direction={isMobile ? "columnReverse" : "row"}
                            justify={"end"}
                            buttons={[
                                {
                                    key: "cancel",
                                    state: "secondaryPure",
                                    label: texts.skipModal.cancel,
                                    size: "medium",
                                    isBlock: isMobile,
                                },
                                {
                                    key: "confirm",
                                    state: "secondary",
                                    label: texts.skipModal.confirm,
                                    size: "medium",
                                    isBlock: isMobile,
                                },
                            ]}
                        />
                    </Margin>
                </Card>
            </Margin>
        </Modal>
    </AbstractSignUpForm>
);

const PasswordCreationContainer = compose<any, IPasswordCreationContainer>(
    withTranslation("pages.signUp.form.passwordCreation"),

    withMobileObserver,
    withState("password", "setPassword", ""),
    withState("passwordConfirmation", "setPasswordConfirmation", ""),
    withState("isModalActive", "setIsModalActive", false),

    withHandlers({
        closeModal: ({setIsModalActive}) => () => {
            setIsModalActive(false);
        },
        showModal: ({setIsModalActive}) => () => {
            setIsModalActive(true);
        },
    }),

    withProps(({isMobile}) => ({
        TitleComponent: isMobile ? H5 : H3,
    })),
)(Inner);

export {
    PasswordCreationContainer,
};
