import React from "react";
import {compose, withState} from "recompose";
import {withMobileObserver} from "../../hoc/withMobileObserver";
import {withTranslation} from "../../hoc/withTranslation";
import {ITranslation} from "../../interfaces/ITranslation";
import {ButtonsList} from "../../templates/components/buttonsList/ButtonsList";
import {Card} from "../../templates/components/card/Card";
import {Form} from "../../templates/components/form/Form";
import {Img} from "../../templates/components/image/Img";
import {H5, P} from "../../templates/components/typography/Typography";
import {Grid, Margin, Row} from "../../templates/layouts/grid/Grid";
import {AbstractSignUpForm} from "../../templates/layouts/signUp/abstract/AbstractSignUpForm";

interface IImportWalletContainer {
    isMobile: boolean;
    texts: ITranslation["pages"]["signUp"]["form"]["importWallet"];

    password: string;
    setPassword(password: string): void;
}

const Inner = ({
    isMobile,
    texts,
    password,
    setPassword,
}: IImportWalletContainer) => (
    <AbstractSignUpForm
        isMobile={isMobile}
        imageType={"import"}
    >
        <Card isMobile={isMobile}>
            <Margin top={isMobile ? 40 : 20}>
                <Row justify={"center"}>
                    <Img
                        url={require("../../assets/images/wallet.png")}
                        type={"block"}
                        borderRadius={65}
                        height={130}
                        width={130}
                    />
                </Row>
            </Margin>
            <Margin top={24}>
                <H5 align={"center"} isBold={true} color={"dark"}>{texts.title}</H5>
            </Margin>
            <Margin top={8}>
                <Grid gap={28}>
                    <P color={"grey"} align={"center"}>{texts.subTitle}</P>
                    <Form.Input
                        type={"password"}
                        placeholder={texts.password}
                        value={password}
                        onInput={setPassword}
                        height={117}
                    />
                    <ButtonsList
                        gap={28}
                        justify={"end"}
                        direction={!isMobile ? "row" : "columnReverse"}
                        buttons={[
                            {
                                key: "back",
                                label: texts.back,
                                state: "secondaryPure",
                                size: "medium",
                                isBlock: isMobile,
                            },
                            {
                                key: "continue",
                                label: texts.continue,
                                state: "secondary",
                                size: "medium",
                                isBlock: isMobile,
                            },
                        ]}
                    />
                </Grid>
            </Margin>
        </Card>
    </AbstractSignUpForm>
);

const ImportWalletContainer = compose<any, IImportWalletContainer>(
    withMobileObserver,
    withTranslation("pages.signUp.form.importWallet"),
    withState("password", "setPassword", ""),
)(Inner);

export {
    ImportWalletContainer,
};
