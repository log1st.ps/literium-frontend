import React from "react";
import {connect} from "react-redux";
import {compose, lifecycle, withProps, withState} from "recompose";
import {withMobileObserver} from "../../hoc/withMobileObserver";
import {withTranslation} from "../../hoc/withTranslation";
import {ITranslation} from "../../interfaces/ITranslation";
import {IUser} from "../../interfaces/IUser";
import {IState} from "../../store/store";
import {Badge} from "../../templates/components/badge/Badge";
import {ButtonsList} from "../../templates/components/buttonsList/ButtonsList";
import {Card} from "../../templates/components/card/Card";
import {Img} from "../../templates/components/image/Img";
import {H5, P} from "../../templates/components/typography/Typography";
import {Grid, Margin, Row} from "../../templates/layouts/grid/Grid";
import {AbstractSignUpForm} from "../../templates/layouts/signUp/abstract/AbstractSignUpForm";

interface IBlockedContainer {
    isMobile?: boolean;
    texts: ITranslation["pages"]["signUp"]["form"]["blocked"];
    avatarUrl: IUser["avatarUrl"];
    userName: IUser["firstName"];

    timeInSeconds: number;
    setTimeInSeconds(timeInSeconds: number): void;

    hours: number;
    minutes: number;
    seconds: number;
}

const Inner = ({
    isMobile,
    texts,
    avatarUrl,
    userName,

    ...props
}: IBlockedContainer) => (
    <AbstractSignUpForm
        isMobile={isMobile}
        imageType={"blocked"}
    >
        <Card isMobile={isMobile}>
            <Margin top={isMobile ? 40 : 20}>
                <Row justify={"center"}>
                    <Img
                        url={avatarUrl}
                        type={"block"}
                        borderRadius={65}
                        height={130}
                        width={130}
                    />
                </Row>
            </Margin>
            <Margin top={24}>
                <H5 align={"center"} isBold={true} color={"dark"}>{texts.title}</H5>
            </Margin>
            <Margin top={8} left={isMobile ? -10 : 30} right={isMobile ? -10 : 30}>
                <Grid gap={isMobile ? 20 : 30}>
                    <Margin left={10} right={10}>
                        <P color={"grey"} align={"center"}>{texts.subTitle.messageFormat({name: userName})}</P>
                    </Margin>
                    <Margin left={isMobile ? 10 : 0} right={isMobile ? 10 : 0}>
                        <Grid gap={isMobile ? 15 : 25} cols={3}>
                            {["hours", "minutes", "seconds"].map((type) => (
                                <Badge
                                    key={type}
                                    state={"timer"}
                                    value={props[type]}
                                    title={texts[type].messageFormat({n: props[type]})}
                                    isMobile={isMobile}
                                />
                            ))}
                        </Grid>
                    </Margin>
                    <Margin top={isMobile ? 10 : 0}>
                        <ButtonsList
                            justify={"center"}
                            buttons={[{
                                key: "back",
                                label: texts.back,
                                state: "secondaryPure",
                            }]}
                        />
                    </Margin>
                </Grid>
            </Margin>
        </Card>
    </AbstractSignUpForm>
);

const BlockedContainer = compose<any, IBlockedContainer>(
    withMobileObserver,
    withTranslation("pages.signUp.form.blocked"),
    connect(
        (state: IState) => ({
            avatarUrl: state.user.user.avatarUrl,
            userName: state.user.user.firstName,
        }),
    ),
    withState("timeInSeconds", "setTimeInSeconds", 15 * 60 * 60),
    withProps(({timeInSeconds: t}) => ({
        hours: Math.floor(t / 60 / 60),
        minutes: Math.floor(t / 60 % 60),
        seconds: Math.floor(t % 60),
    })),
    lifecycle({
        componentDidMount() {
            const {
                setTimeInSeconds,
            } = this.props as IBlockedContainer;

            const interval = setInterval(() => {
                const {timeInSeconds} = this.props as IBlockedContainer;
                if (timeInSeconds === 1) {
                    clearInterval(interval);
                }
                setTimeInSeconds(timeInSeconds - 1);
            }, 1000);
        },
    }),
)(Inner);

export {
    BlockedContainer,
};
