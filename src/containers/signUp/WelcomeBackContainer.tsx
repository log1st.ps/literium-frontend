import React from "react";
import {connect} from "react-redux";
import {compose, lifecycle, withHandlers, withState} from "recompose";
import {withMobileObserver} from "../../hoc/withMobileObserver";
import {withTranslation} from "../../hoc/withTranslation";
import {ITranslation} from "../../interfaces/ITranslation";
import {IUser} from "../../interfaces/IUser";
import {IState} from "../../store/store";
import {ButtonsList} from "../../templates/components/buttonsList/ButtonsList";
import {Card} from "../../templates/components/card/Card";
import {Form} from "../../templates/components/form/Form";
import {Img} from "../../templates/components/image/Img";
import {H5, P} from "../../templates/components/typography/Typography";
import {Grid, Margin, Row} from "../../templates/layouts/grid/Grid";
import {AbstractSignUpForm} from "../../templates/layouts/signUp/abstract/AbstractSignUpForm";

interface IWelcomeBackContainer {
    isMobile: boolean;
    userName: IUser["firstName"];
    avatarUrl: IUser["avatarUrl"];
    texts: ITranslation["pages"]["signUp"]["form"]["welcomeBack"];

    password: string;
    setPassword(password: string): void;
    error: string;
    setError(error: string): void;

    onSubmit(): void;
}

const Inner = ({
    isMobile,
    avatarUrl,
    userName,
    texts,
    password,
    setPassword,
    error,
    onSubmit,
}: IWelcomeBackContainer) => (
    <AbstractSignUpForm
        isMobile={isMobile}
        imageType={"password2"}
    >
        <Card isMobile={isMobile}>
            <Margin top={isMobile ? 40 : 20}>
                <Row justify={"center"}>
                    <Img
                        url={avatarUrl}
                        type={"block"}
                        borderRadius={65}
                        height={130}
                        width={130}
                    />
                </Row>
            </Margin>
            <Margin top={24}>
                <H5 align={"center"} isBold={true} color={"dark"}>{texts.title.messageFormat({name: userName})}</H5>
            </Margin>
            <Margin top={8}>
                <Grid gap={28}>
                    <P color={"grey"} align={"center"}>{texts.subTitle}</P>
                    <Form.Input
                        type={"password"}
                        placeholder={texts.password}
                        value={password}
                        onInput={setPassword}
                        error={error}
                        height={117}
                    />
                    <ButtonsList
                        gap={28}
                        justify={"end"}
                        direction={!isMobile ? "row" : "columnReverse"}
                        buttons={[
                            {
                                key: "back",
                                label: texts.switch,
                                state: "secondaryPure",
                                size: "medium",
                                isBlock: isMobile,
                            },
                            {
                                key: "continue",
                                label: texts.continue,
                                state: "secondary",
                                size: "medium",
                                isBlock: isMobile,
                                onClick: onSubmit,
                            },
                        ]}
                    />
                </Grid>
            </Margin>
        </Card>
    </AbstractSignUpForm>
);

const WelcomeBackContainer = compose<any, IWelcomeBackContainer>(
    withMobileObserver,
    withTranslation("pages.signUp.form.welcomeBack"),
    withState("password", "setPassword", ""),
    withState("error", "setError", ""),
    withHandlers({
        onSubmit: ({setError, texts}: IWelcomeBackContainer) => () => {
            setError(texts.errors.incorrect);
        },
    }),
    connect(
        (state: IState) => ({
            avatarUrl: state.user.user.avatarUrl,
            userName: state.user.user.firstName,
        }),
    ),
    lifecycle({
        shouldComponentUpdate({password, setError}) {
            const {
                password: oldPassword,
            } = this.props as IWelcomeBackContainer;
            if (password !== oldPassword) {
                setError("");
            }
            return true;
        },
    }),
)(Inner);

export {
    WelcomeBackContainer,
};
