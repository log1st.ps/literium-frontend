import React from "react";
import {compose, withState} from "recompose";
import {withMobileObserver} from "../../hoc/withMobileObserver";
import {withTranslation} from "../../hoc/withTranslation";
import {ITranslation} from "../../interfaces/ITranslation";
import {Alert} from "../../templates/components/alert/Alert";
import {ButtonsList} from "../../templates/components/buttonsList/ButtonsList";
import {Divider} from "../../templates/components/divider/Divider";
import {Form} from "../../templates/components/form/Form";
import {H3, H6, P} from "../../templates/components/typography/Typography";
import {Col, Grid, Row} from "../../templates/layouts/grid/Grid";
import {AbstractSignUpForm} from "../../templates/layouts/signUp/abstract/AbstractSignUpForm";

interface IPhraseGenerationContainer {
    texts: ITranslation["pages"]["signUp"]["form"]["phraseGeneration"];
    isAgreed: boolean;
    setIsAgreed(isAgreed: boolean): void;
    isMobile?: boolean;
}

const Inner = ({
    texts,
    isAgreed,
    setIsAgreed,
    isMobile,
}: IPhraseGenerationContainer) => (
    <AbstractSignUpForm
        isMobile={isMobile}
        imageType={"passwordPhrase"}
    >
        <Grid gap={isMobile ? 28 : 41}>
            <Grid gap={isMobile ? 20 : 28}>
                <H3 isBold={true}>
                    {texts.title.split("\n").map(((item, index) => (
                        <div key={index}>{item}</div>
                    )))}
                </H3>
                <Row direction={isMobile ? "column" : "row"} gap={isMobile ? 20 : 50}>
                    <Col direction={isMobile ? "column" : "row"} gap={isMobile ? 20 : 50} isGrow={true}>
                        <H6>{texts.copy}</H6>
                    </Col>
                    <Col direction={isMobile ? "column" : "row"} gap={isMobile ? 20 : 50} isShrink={false}>
                        <ButtonsList
                            buttons={[
                                {
                                    key: "copy",
                                    label: texts.copyButton,
                                    size: "small",
                                },
                            ]}
                        />
                    </Col>
                </Row>
                {isMobile && (
                    <Divider margin={12}/>
                )}
                <Alert>
                    <P>{texts.alert}</P>
                </Alert>
                <Form>
                    <Form.Checkbox
                        isMobile={isMobile}
                        value={isAgreed}
                        onInput={setIsAgreed}
                        label={texts.agreement}
                    />
                </Form>
            </Grid>
            <ButtonsList
                gap={28}
                justify={"end"}
                direction={!isMobile ? "row" : "columnReverse"}
                buttons={[
                    {
                        key: "back",
                        label: texts.back,
                        state: "pure",
                        size: "medium",
                        isBlock: isMobile,
                    },
                    {
                        key: "continue",
                        label: texts.continue,
                        state: "secondary",
                        size: "medium",
                        isBlock: isMobile,
                    },
                ]}
            />
        </Grid>
    </AbstractSignUpForm>
);

const PhraseGenerationContainer = compose<any, IPhraseGenerationContainer>(
    withTranslation("pages.signUp.form.phraseGeneration"),

    withMobileObserver,
    withState("isAgreed", "setIsAgreed", false),
)(Inner);

export {
    PhraseGenerationContainer,
};
