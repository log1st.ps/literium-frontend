import React from "react";
import {compose, withProps} from "recompose";
import {withMobileObserver} from "../hoc/withMobileObserver";
import {IMainLayout, MainLayout} from "../templates/layouts/main/MainLayout";
import {HeaderContainer} from "./HeaderContainer";
import {SideContainer} from "./SideContainer";

const MainLayoutContainer = compose<any, any>(
    withProps<IMainLayout, IMainLayout>({
        renderSide: <SideContainer/>,
        renderHeader: <HeaderContainer/>,
    }),
    withMobileObserver,
)(MainLayout);

export {
    MainLayoutContainer,
};
