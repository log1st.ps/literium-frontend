import {ReactComponentLike} from "prop-types";
import React from "react";
import {ITranslation} from "../interfaces/ITranslation";
import {ButtonsList} from "../templates/components/buttonsList/ButtonsList";
import {P} from "../templates/components/typography/Typography";
import {Icon} from "../templates/elements/icon/Icon";
import {availableSignUpSteps} from "../templates/layouts/signUp/base/SignUpLayout";

export interface ISignUpPage {
    texts: ITranslation["pages"]["signUp"];
    children?: any;
    isMobile?: boolean;
    isForm?: boolean;
    TitleComponent: ReactComponentLike;
    HintComponent: ReactComponentLike;
    FormContainer?: ReactComponentLike;

    step: typeof availableSignUpSteps[number];
    hint: string;
}

const getLandingAndLoadersProps = ({
   texts,
   isMobile,
   TitleComponent,
   HintComponent,
   step,
   hint,
}: ISignUpPage) => ({
    renderSideActions: step === "initial" && (
        <ButtonsList
            buttons={[
                {
                    key: "signIn",
                    label: texts.signIn,
                    state: isMobile ? "default" : "outlineLight",
                    size: isMobile ? "medium" : "default",
                    isBlock: isMobile,
                },
            ]}
        />
    ),
    renderTitle: step === "initial" && (
        <TitleComponent
            isBold={!isMobile}
            isMedium={isMobile}
            align={"center"}
        >
            {
                !isMobile ? (
                    texts.title.split("\n").map((line, index) => (<div key={index}>{line}</div>))
                ) : texts.title.replace(/\n/g, " ")
            }
        </TitleComponent>
    ),
    renderActions: step === "initial" && (
        <ButtonsList
            gap={isMobile ? 20 : 32}
            direction={isMobile ? "column" : "row"}
            buttons={[
                {
                    key: "create",
                    state: "secondary",
                    size: isMobile ? "medium" : "big",
                    label: texts.createWallet,
                    isBlock: isMobile,
                    isOneLine: true,
                    renderBefore: (
                        <Icon
                            type={"walletPlus"}
                            size={"sm"}
                        />
                    ),
                },
                {
                    key: "import",
                    state: "default",
                    size: isMobile ? "medium" : "big",
                    label: texts.importWallet,
                    isBlock: isMobile,
                    isOneLine: true,
                    renderBefore: (
                        <Icon
                            type={"walletIn"}
                            size={"sm"}
                        />
                    ),
                },
            ]}
        />
    ),
    renderHint: step !== "initial" && (
        <HintComponent align={"center"}>
            {!isMobile ? texts.step[step].replace(/\n/g, " ") : (
                texts.step[step].split("\n").map((block, i) => (<div key={i}>{block}</div>))
            )}
        </HintComponent>
    ),
    imageType: step !== "initial" && step,
    renderPreloader: step !== "initial" && ["loginSuccess", "created", "imported"].indexOf(step) > -1 && (
        <P>
            {hint}
        </P>
    ),
    withBackground: step === "initial",
});

export {
    getLandingAndLoadersProps,
};
