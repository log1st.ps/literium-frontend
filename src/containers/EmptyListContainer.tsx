import React from "react";
import {ButtonsList} from "../templates/components/buttonsList/ButtonsList";
import {Img} from "../templates/components/image/Img";
import {H5, P} from "../templates/components/typography/Typography";
import {Icon, IIcon} from "../templates/elements/icon/Icon";
import {Grid, Margin, Row} from "../templates/layouts/grid/Grid";

interface IEmptyListContainer {
    image?: string;
    title?: string;
    subTitle?: string;
    buttonLabel?: string;
    buttonIcon?: IIcon["type"];
    onClick?(): void;
    isMobile?: boolean;
    imgLeft?: number;
    imgTop?: number;
}

const EmptyListContainer = ({
    image,
    title,
    subTitle,
    buttonLabel,
    onClick,
    isMobile,
    buttonIcon,
    imgLeft,
    imgTop,
}: IEmptyListContainer) => (
    <Margin top={isMobile ? 50 : -15}>
        <Grid>
            <Margin left={imgLeft} top={imgTop}>
                <Img
                    url={image}
                    height={isMobile ? 150 : 185}
                    width={"100%"}
                    alt={title}
                    type={"block"}
                />
            </Margin>
            <Margin top={48}>
                <H5 isBold={true} align={"center"}>{title}</H5>
            </Margin>
            <Margin top={8}>
                <P align={"center"}>
                    {
                        isMobile
                            ? subTitle.replace(/\n/g, " ")
                            : subTitle.split("\n").map((i, index) => (<div key={index}>{i}</div>))
                    }
                </P>
            </Margin>
            <Margin top={isMobile ? 38 : 36}>
                <ButtonsList
                    justify={"center"}
                    buttons={[{
                        key: "cta",
                        state: "secondary",
                        size: isMobile ? "medium" : "big",
                        label: buttonLabel,
                        onClick,
                        renderBefore: buttonIcon && (
                            <Icon type={buttonIcon} size={"xs"}/>
                        ),
                    }]}
                />
            </Margin>
        </Grid>
    </Margin>
);

export {
    EmptyListContainer,
};
