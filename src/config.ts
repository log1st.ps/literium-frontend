import getConfig from "next/config";

const {publicRuntimeConfig: {
    isDevelopment,
    datepickerFormat,
}} = getConfig();

export {
    isDevelopment,
    datepickerFormat,
};
