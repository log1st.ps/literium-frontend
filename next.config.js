const withScss = require("@zeit/next-sass");
const withOffline = require("next-offline");
const path = require("path");
const configurator = require('./common.config')

require("dotenv").config();
const Dotenv = require("dotenv-webpack");

const config = {
    cssModules: true,
    cssLoaderOptions: {
        importLoaders: 1,
        localIdentName: "[local]___[hash:base64:5]",
    },
    webpack(config, options) {
        const { isServer } = options;

        config = configurator.webpack(config, {
            isServer
        })

        config.plugins.push(new Dotenv({
            path: path.join(__dirname, ".env"),
            systemvars: true,
        }));

        return config;
    },
    publicRuntimeConfig: {
        isDevelopment: process.env.NODE_ENV,
        apiUrl: process.env.API_URL,

        datepickerFormat: process.env.DATEPICKER_FORMAT,
    },
};

module.exports = withOffline(withScss(config));
