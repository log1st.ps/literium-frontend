import React from "react";
import {Icon, iconSizes, iconTypes} from "../../../../src/templates/elements/icon/Icon";
import {createStories} from "../../../helpers/createStories";
import {select} from "../../../helpers/knobs";

createStories("Icon", {
    default: {
        Component: Icon,
        knobs: {
            type: select("type", "angleBottom", Array.from(iconTypes)),
            size: select("size", "xs", Array.from(iconSizes)),
        },
    },
});
