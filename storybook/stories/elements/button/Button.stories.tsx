import {Button} from "../../../../src/templates/elements/button/Button";
import {createStories} from "../../../helpers/createStories";
import {select, text} from "../../../helpers/knobs";

createStories("Button", {
    default: {
        Component: Button,
        actions: [
            "onClick",
        ],
        knobs: {
            size: select("size", "default", ["small", "default", "medium", "big"]),
            state: select("size", "default", ["action", "default", "outline", "outlineLight", "secondary"]),
            children: text("children", "Label"),
        },
    },
});
