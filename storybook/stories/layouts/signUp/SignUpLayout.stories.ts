import {SignUpLayout} from "../../../../src/templates/layouts/signUp/base/SignUpLayout";
import {createStories} from "../../../helpers/createStories";
import {checkbox, text} from "../../../helpers/knobs";

createStories("Layouts", {
    'Sign Up': {
        Component: SignUpLayout,
        knobs: {
            renderLogo: text("renderLogo", "Logo"),
            renderSideActions: text("renderSdeActions", "Side Actions"),
            renderTitle: text("renderTitle", "Title"),
            renderActions: text("renderActions", "Actions"),
            isMobile: checkbox("isMobile", false),
            withBackground: checkbox("withBackground", true),
        },
    },
});
