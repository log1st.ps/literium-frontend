import {Logo} from "../../../../src/templates/components/logo/Logo";
import {createStories} from "../../../helpers/createStories";
import {text} from "../../../helpers/knobs";

createStories("Logo", {
    default: {
        Component: Logo,
        knobs: {
            height: text("height", 21, "number"),
            width: text("width", 88, "number"),
        },
    },
});
