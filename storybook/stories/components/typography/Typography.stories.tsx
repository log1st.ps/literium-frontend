import React, { Fragment } from "react";
import {ICommonTypography} from "../../../../src/templates/components/typography/Typography";
import * as components from "../../../../src/templates/components/typography/Typography";
import {createStories} from "../../../helpers/createStories";
import {checkbox, select, text} from "../../../helpers/knobs";

const Component = (props: ICommonTypography) => (
    <div>
        {Object.entries(components).map(([key, TypographyComponent]) => (
            <Fragment key={key}>
                <TypographyComponent {...props}/>
                <br/>
            </Fragment>
        ))}
    </div>
);

createStories("Typography", {
    default: {
        Component,
        knobs: {
            text: text("text", "Hello"),
            isBold: checkbox("isBold", false),
            align: select("align", "left", ["left", "center", "right"]),
        },
    },
});
