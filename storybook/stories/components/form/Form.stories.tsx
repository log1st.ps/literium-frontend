import {Checkbox} from "../../../../src/templates/components/form/elements/checkbox/Checkbox";
import {Input} from "../../../../src/templates/components/form/elements/input/Input";
import {Form} from "../../../../src/templates/components/form/Form";
import {createStories} from "../../../helpers/createStories";
import {checkbox, select, text} from "../../../helpers/knobs";

const knobs = {
    label: text("label", "Label"),
    error: text("error", "Error"),
    placeholder: text("placeholder", "Placeholder"),
    isDisabled: checkbox("isDisabled", false),
    isReadonly: checkbox("isReadonly", false),
    isMobile: checkbox("isMobile", false),
    before: text("before", "Before"),
    after: text("after", "After"),
    hint: text("hint", "Hint"),
    hasAppearance: checkbox("hasAppearance", true),
    isLight: checkbox("isLight", false),
    height: text("height", "auto"),
    width: text("width", "100%"),
    isOutsideFocused: checkbox("isOutsideFocused", false),
};

const actions = [
    "onInput",
    "onFocus",
    "onBlur",
];

createStories("Form", {
    default: {
        Component: Form,
        knobs: {
            children: text("children", "Form children"),
        },
    },
    input: {
        Component: Input,
        knobs: {
            value: text("value", "Value"),
            type: select("type", "text", ["text", "number", "textarea", "password"]),
            ...knobs,
        },
        actions: [
            ...actions,
        ],
    },
    checkbox: {
        Component: Checkbox,
        knobs: {
            value: text("value", "1"),
            trueValue: text("trueValue", "1"),
            falseValue: text("falseValue", "0"),
            ...knobs,
        },
        actions: [
            ...actions,
        ],
    },
});
