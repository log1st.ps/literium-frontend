export interface IKnob {
    name: string;
    value: any;
    type: "select" | "text" | "checkbox" | string;
    config: {
        [key: string]: any,
    };
}

const select = (
    name: string,
    value: string | number | string[] | number[],
    values?: string[] | number[] | {
        [key: number]: string | number,
    } | {
        [key: string]: string | number,
    },
): IKnob => ({
    name,
    value,
    type: "select",
    config: {
        options: values,
    },
});

const text = (
    name: string,
    value: string | number,
    type: "text" | "number" | "textarea" = "text",
) => ({
    name,
    value,
    type: "text",
    config: {
        type,
    },
});

const checkbox = (
    name: string,
    value: boolean,
    trueValue: any = true,
    falseValue: any = false,
) => ({
    name,
    value,
    type: "checkbox",
    config: {
        trueValue,
        falseValue,
    },
});

export {
    select,
    text,
    checkbox,
};
