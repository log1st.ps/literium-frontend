import {IStory, stories} from "../Storybook";

const pushStory = (name: string, key: string, story: any) => {
    if (!(name in stories)) {
        stories[name] = {};
    }

    stories[name][key] = story;
};

const createStories = (name: string, components: {
    [key: string]: IStory,
}) => {
    Object.entries(components).forEach(([key, {Component, knobs, actions}]) => {
        pushStory(name, key, {
            Component,
            actions: actions || [],
            knobs: knobs || {},
        });
    });
};

export {
    createStories,
};
