import classNames from "classnames";
import {NextRouter, useRouter} from "next/router";
import React, { Fragment } from "react";
import {compose, lifecycle, withHandlers, withProps, withState} from "recompose";
import {IKnob} from "./helpers/knobs";
import styles from "./storybook.scss";

const stories: IInnerStorybook["storiesTree"] = {};

((r) => {
    r.keys().forEach((key) => r(key));
})(
    // @ts-ignore
    require.context("./stories", true, /\.stories\.ts(x?)$/),
);

export interface IStory {
    knobs?: {
        [key: string]: IKnob;
    };
    actions?: string[];
    Component: any;
}

interface IStoryAction {
    key: string;
    args: string;
}

interface IStorybook {
    storyOnly?: boolean;
}

interface IInnerStorybook extends IStorybook {
    storiesTree: {
        [key: string]: {
            [name: string]: IStory,
        },
    };

    component: string;
    setComponent(component: string): () => void;

    variant: string;
    setVariant(variant: string): () => void;

    actions: IStoryAction[];
    setActions(actions: IStoryAction[]): void;

    addAction(action: string): void;

    router: NextRouter;

    story: IStory;
    setStory(story: IStory): void;

    state: {
        [key: string]: any;
    };
    setState(state: {
        [key: string]: any;
    }): void;

    barTab: "knobs" | "actions";
    setBarTab(barTab: "knobs" | "actions"): () => void;

    updateStateField(key: string): () => void;

    background: string;
    setBackground(background: string): () => void;
}

const StorybookInner = ({
    storiesTree,
    component,
    setComponent,
    variant,
    setVariant,
    story,
    addAction,
    state,
    barTab,
    setBarTab,
    updateStateField,
    actions,
    setBackground,
    background,
    storyOnly,
    router,
}: IInnerStorybook) => (
    <div className={styles.layout}>
        {!storyOnly && (
            <div className={styles.header}>
                <h1>Storybook</h1>
            </div>
        )}
        <div className={styles.body}>
            {!storyOnly && (
                <div className={styles.side}>
                    <ul className={styles.components}>
                        {Object.keys(storiesTree).map((key) => (
                            <li
                                key={key}
                                className={styles.component}
                            >
                                <button
                                    className={classNames({
                                        [styles.componentLink]: true,
                                        [styles.activeComponentLink]: key === component,
                                    })}
                                    onClick={setComponent(key)}
                                >
                                    {key}
                                </button>
                                {key === component && (
                                    <ul className={styles.variants}>
                                        {Object.keys(storiesTree[key]).map((subKey) => (
                                            <li
                                                key={subKey}
                                                className={styles.variant}
                                            >
                                                <button
                                                    className={classNames({
                                                        [styles.variantLink]: true,
                                                        [styles.activeVariantLink]: subKey === variant,
                                                    })}
                                                    onClick={setVariant(subKey)}
                                                >
                                                    {subKey}
                                                </button>
                                            </li>
                                        ))}
                                    </ul>
                                )}
                            </li>
                        ))}
                    </ul>
                    <div className={styles.bgs}>
                        {["transparent", "design", "black", "white", "grey"].map((item) => (
                            <button
                                key={item}
                                className={classNames({
                                    [styles.bgButton]: true,
                                    [styles.bgButtonActive]: item === background,
                                })}
                                onClick={setBackground(item)}
                            >
                                {item}
                            </button>
                        ))}
                    </div>
                </div>
            )}
            <div className={styles.content}>
                <div
                    className={classNames({
                        [styles.story]: true,
                        [styles.storyOnly]: storyOnly,
                        [styles[`story${background[0].toUpperCase() + background.substr(1)}`]]: true,
                    })}
                >
                    {(story && (Object.entries(state).length === Object.entries(story.knobs).length)) ? (
                        <Fragment>
                            {storyOnly ? (
                                <div>
                                    <story.Component
                                        {
                                            ...Object.entries(story.knobs).reduce((prev, [key]) => ({
                                                ...prev,
                                                [key]: state[key],
                                            }), {})
                                        }
                                        {
                                            ...(story.actions || []).reduce((prev, event) => ({
                                                ...prev,
                                                [event]: addAction(event),
                                            }), {})
                                        }
                                    />
                                </div>
                            ) : (
                                <iframe
                                    style={{height: "100%", width: "100%"}}
                                    src={router.asPath.replace("/storybook", "/storybook/story")}
                                />
                            )}
                        </Fragment>
                    ) : (
                        <div className={styles.variantsList}>
                            {component && (
                                <Fragment>
                                    <h3 className={styles.variantsTitle}>Variants:</h3>
                                    {Object.keys(storiesTree[component]).map((key, index) => (
                                        <div key={key}>
                                            <button
                                                className={styles.variantItem}
                                                onClick={setVariant(key)}
                                            >
                                                #{index + 1}: {key}
                                            </button>
                                        </div>
                                    ))}
                                </Fragment>
                            )}
                        </div>
                    )}
                </div>
                {!storyOnly && (
                    <div className={styles.bar} style={{height: "300px"}}>
                        {(story) && (
                            <div className={styles.barTabs}>
                                {["knobs", "actions"].map((tab: "knobs" | "actions") => (
                                    <button
                                        key={tab}
                                        className={classNames({
                                            [styles.barTab]: true,
                                            [styles.barTabActive]: barTab === tab,
                                        })}
                                        onClick={setBarTab(tab)}
                                    >
                                        {tab[0].toUpperCase() + tab.substr(1)}
                                    </button>
                                ))}
                            </div>
                        )}
                        <div className={styles.barPanes}>
                            {(story) && (
                                <Fragment>
                                    {(barTab === "knobs" && Object.entries(state).length > 0) && (
                                        <div className={styles.barPane}>
                                            {Object.entries(story.knobs).map(([key, knob]) => (
                                                <div
                                                    key={key}
                                                    className={styles.knob}
                                                >
                                                    {["checkbox"].indexOf(knob.type) === -1 && (
                                                        <div className={styles.knobTitle}>{knob.name}</div>
                                                    )}
                                                    <div className={styles.knobBody}>
                                                        {{
                                                            select: () => (
                                                                <select
                                                                    value={state[key]}
                                                                    className={styles.selectKnob}
                                                                    onChange={updateStateField(key)}
                                                                >
                                                                    {knob.config.options.map((value) => (
                                                                        <option
                                                                            key={value}
                                                                            value={value}
                                                                        >
                                                                            {value}
                                                                        </option>
                                                                    ))}
                                                                </select>
                                                            ),
                                                            text: () => (
                                                                knob.config.type === "textarea" ? (
                                                                    <textarea
                                                                        className={styles.inputKnob}
                                                                        onChange={updateStateField(key)}
                                                                    >
                                                                        {state[key]}
                                                                    </textarea>
                                                                ) : (
                                                                    <input
                                                                        value={state[key]}
                                                                        className={styles.inputKnob}
                                                                        type={knob.config.type}
                                                                        onChange={updateStateField(key)}
                                                                    />
                                                                )
                                                            ),
                                                            checkbox: () => (
                                                                <label
                                                                    className={classNames({
                                                                        [styles.checkboxKnob]: true,
                                                                        [styles.checkboxKnobChecked]: state[key],
                                                                    })}
                                                                >
                                                                    <input
                                                                        type={"checkbox"}
                                                                        checked={state[key] === knob.config.trueValue}
                                                                        onChange={updateStateField(key)}
                                                                    />
                                                                    {knob.name}
                                                                </label>
                                                            ),
                                                        }[knob.type]()}
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    )}
                                    {barTab === "actions" && (
                                        <div className={styles.barPane}>
                                            <div className={styles.actionsList}>
                                                {actions.reverse().map(({key, args}, index) => (
                                                    <div
                                                        key={index}
                                                        className={styles.actionItem}
                                                    >
                                                        #{actions.length - index} - <b>{key}:</b> {args}
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                    )}
                                </Fragment>
                            )}
                        </div>
                    </div>
                )}
            </div>
        </div>
    </div>
);

const Storybook = compose<any, any>(
    withProps(() => ({
        storiesTree: stories,
        router: useRouter(),
    })),
    withState("component", "setComponent", null),
    withState("variant", "setVariant", null),
    withState("story", "setStory", null),
    withState("state", "setState", {}),
    withState("actions", "setActions", []),
    withState("barTab", "setBarTab", "knobs"),
    withState("background", "setBackground", "transparent"),
    withHandlers({
        setBackground: ({setBackground, router}) => (background: string) => () => {
            setBackground(background);

            if (router.query.background !== background) {
                router.push({
                    pathname: router.pathname,
                    query: {
                        ...router.query,
                        background,
                    },
                });
            }
        },
        setState: ({setState, router}: IInnerStorybook) => (state: IInnerStorybook["state"]) => {
            setState(state);

            if (!state) {
                return;
            }

            const JSONState = JSON.stringify(state);
            const JSONRouterState = JSON.stringify(router.query.state);

            if (JSONState !== JSONRouterState) {
                router.push({
                    pathname: router.pathname,
                    query: {
                        ...router.query,
                        state: JSONState,
                    },
                });
            }
        },
    }),
    withHandlers({
        addAction: ({actions, setActions}) => (action: string) => (...args) => {
            setActions([
                ...actions,
                {
                    key: action,
                    args: JSON.stringify(args),
                },
            ]);
        },
        setComponent: ({
            setComponent,
            setStory,
            setVariant,
            router,
            setState,
        }: IInnerStorybook) => (component: string) => () => {
            setComponent(component);
            setVariant(null);
            setStory(null);
            setState({});

            if (router.query.component !== component) {
                router.push({
                    pathname: router.pathname,
                    query: {
                        ...router.query,
                        component,
                        variant: null,
                    },
                });
            }
        },
        setVariant: ({
            setVariant,
            setStory,
            storiesTree,
            component,
            setState,
            router,
        }: IInnerStorybook) => (variant: string) => () => {
            setVariant(variant);

            const story = storiesTree[component][variant];
            setStory(story);

            setState(
                Object.entries((story.knobs) as IStory["knobs"]).reduce((prev, [key, {value}]) => ({
                    ...prev,
                    [key]: value,
                }), {}),
            );

            if (router.query.variant !== variant) {
                router.push({
                    pathname: router.pathname,
                    query: {
                        component,
                        variant,
                        background: router.query.background || null,
                    },
                });
            }
        },
        setBarTab: ({setBarTab}) => (barTab: string) => () => {
            setBarTab(barTab);
        },
        updateStateField: ({state, setState, story}) => (key: string) => (e) => {
            const {type, config} = story.knobs[key];
            const value = type === "checkbox"
                ? (state[key] === config.trueValue ? config.falseValue : config.trueValue)
                : e.target.value;

            setState({
                ...state,
                [key]: value,
            });
        },
    }),
    lifecycle({
        componentDidMount() {
            setTimeout(() => {
                const {
                    router: {
                        query: {
                            component,
                            variant,
                            state,
                            background,
                        },
                    },
                    setComponent,
                    setVariant,
                    setState,
                    setBackground,
                } = this.props as IInnerStorybook;

                if (component) {
                    setComponent(component as string)();
                }

                if (variant) {
                    setVariant(variant as string)();
                }

                if (state) {
                    setState(JSON.parse(state as string));
                }

                if (background) {
                    setBackground(background as string)();
                }
            });
        },
    }),
)(StorybookInner);

export {
    Storybook,
    stories,
};
