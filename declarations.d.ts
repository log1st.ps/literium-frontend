declare module "*.scss";

declare module "*.inline.svg" {
    import React from "react";
    const value: typeof React.Component;
    export = value;
}
